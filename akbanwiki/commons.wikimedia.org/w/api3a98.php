<?xml version="1.0"?>
<feed xmlns="http://www.w3.org/2005/Atom" xml:lang="en">
		<id>https://commons.wikimedia.org/w/api.php?hidebots=1&amp;hidecategorization=1&amp;days=7&amp;limit=50&amp;userExpLevel=all&amp;hideWikibase=1&amp;action=feedrecentchanges&amp;feedformat=atom</id>
		<title>Wikimedia Commons  - Recent changes [en]</title>
		<link rel="self" type="application/atom+xml" href="https://commons.wikimedia.org/w/api.php?hidebots=1&amp;hidecategorization=1&amp;days=7&amp;limit=50&amp;userExpLevel=all&amp;hideWikibase=1&amp;action=feedrecentchanges&amp;feedformat=atom"/>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/wiki/Special:RecentChanges"/>
		<updated>2017-03-04T21:45:50Z</updated>
		<subtitle>Track the most recent changes to the wiki in this feed.</subtitle>
		<generator>MediaWiki 1.29.0-wmf.14</generator>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:Heidenau,_Germany_-_panoramio_(4).jpg&amp;diff=236000063&amp;oldid=0</id>
		<title>File:Heidenau, Germany - panoramio (4).jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:Heidenau,_Germany_-_panoramio_(4).jpg&amp;diff=236000063&amp;oldid=0"/>
				<updated>2017-03-04T21:45:48Z</updated>
		
		<summary type="html">&lt;p&gt;&lt;a href=&quot;/wiki/User:Panoramio_upload_bot&quot; class=&quot;mw-userlink&quot; title=&quot;User:Panoramio upload bot&quot;&gt;&lt;bdi&gt;Panoramio upload bot&lt;/bdi&gt;&lt;/a&gt; uploaded &lt;a href=&quot;/wiki/File:Heidenau,_Germany_-_panoramio_(4).jpg&quot; title=&quot;File:Heidenau, Germany - panoramio (4).jpg&quot;&gt;File:Heidenau, Germany - panoramio (4).jpg&lt;/a&gt; == {{int:filedesc}} == {{Information |description=Heidenau, Germany |date={{Taken on|2013-08-07}} |source=http://www.panoramio.com/photo/94315920 |author=[http://www.panoramio.com/user/7778207?with_photo_id=94315920 Kalispera Dell] |permission={{cc-by-...&lt;/p&gt;
&lt;p&gt;&lt;b&gt;New page&lt;/b&gt;&lt;/p&gt;&lt;div&gt;== {{int:filedesc}} ==&lt;br /&gt;
{{Information&lt;br /&gt;
|description=Heidenau, Germany&lt;br /&gt;
|date={{Taken on|2013-08-07}}&lt;br /&gt;
|source=http://www.panoramio.com/photo/94315920&lt;br /&gt;
|author=[http://www.panoramio.com/user/7778207?with_photo_id=94315920 Kalispera Dell]&lt;br /&gt;
|permission={{cc-by-3.0|Kalispera Dell}}&lt;br /&gt;
{{Panoramioreview|Panoramio_upload_bot|2017-03-04}}&lt;br /&gt;
|other_versions=&lt;br /&gt;
|other_fields={{Information field|Name=Tags&amp;lt;br /&amp;gt;(from Panoramio photo page)|Value=&amp;lt;code&amp;gt;Heidenau&amp;lt;/code&amp;gt;, &amp;lt;code&amp;gt;Heidenau&amp;lt;/code&amp;gt;}}&lt;br /&gt;
}}&lt;br /&gt;
{{Location|50.98013|13.8528|source:Panoramio}}&lt;br /&gt;
&lt;br /&gt;
{{Check categories|year=2017|month=March|day=4}}&lt;br /&gt;
&lt;br /&gt;
[[Category:Heidenau]]&lt;br /&gt;
[[Category:Heidenau]]&lt;br /&gt;
[[Category:Panoramio files uploaded by Panoramio_upload_bot]]&lt;/div&gt;</summary>
		<author><name>Panoramio upload bot</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:US_Secret_Service_Uniformed_Division_Ford_Taurus-Police_Interceptor_at_White_House_(17011433467).jpg&amp;diff=236000061&amp;oldid=0</id>
		<title>File:US Secret Service Uniformed Division Ford Taurus-Police Interceptor at White House (17011433467).jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:US_Secret_Service_Uniformed_Division_Ford_Taurus-Police_Interceptor_at_White_House_(17011433467).jpg&amp;diff=236000061&amp;oldid=0"/>
				<updated>2017-03-04T21:45:44Z</updated>
		
		<summary type="html">&lt;p&gt;&lt;a href=&quot;/wiki/User:XXN&quot; class=&quot;mw-userlink&quot; title=&quot;User:XXN&quot;&gt;&lt;bdi&gt;XXN&lt;/bdi&gt;&lt;/a&gt; uploaded &lt;a href=&quot;/wiki/File:US_Secret_Service_Uniformed_Division_Ford_Taurus-Police_Interceptor_at_White_House_(17011433467).jpg&quot; title=&quot;File:US Secret Service Uniformed Division Ford Taurus-Police Interceptor at White House (17011433467).jpg&quot;&gt;File:US Secret Service Uniformed Division Ford Taurus-Police Interceptor at White House (17011433467).jpg&lt;/a&gt; Transferred from Flickr via &lt;a href=&quot;/wiki/Commons:Flickr2Commons&quot; title=&quot;Commons:Flickr2Commons&quot;&gt;Flickr2Commons&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;New page&lt;/b&gt;&lt;/p&gt;&lt;div&gt;== {{int:filedesc}} ==&lt;br /&gt;
{{Information&lt;br /&gt;
| Description = US Secret Service Uniformed Division Ford Taurus/Police Interceptor at White House&lt;br /&gt;
| Source      = [https://www.flickr.com/photos/_schmal/17011433467/ US Secret Service Uniformed Division Ford Taurus/Police Interceptor at White House]&lt;br /&gt;
| Date        = 2015-04-17 09:43&lt;br /&gt;
| Author      = [https://www.flickr.com/people/132192110@N04 Alex Smith] from Fort Collins, CO, United States&lt;br /&gt;
| Permission  = &lt;br /&gt;
| other_versions=&lt;br /&gt;
}}&lt;br /&gt;
&lt;br /&gt;
=={{int:license-header}}==&lt;br /&gt;
{{cc-zero}}&lt;br /&gt;
{{flickrreview}}&lt;br /&gt;
[[Category:Photographs by Alex Smith]]&lt;/div&gt;</summary>
		<author><name>XXN</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:Hazlehurst_Post_Office.JPG&amp;diff=236000059&amp;oldid=224262453</id>
		<title>File:Hazlehurst Post Office.JPG</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:Hazlehurst_Post_Office.JPG&amp;diff=236000059&amp;oldid=224262453"/>
				<updated>2017-03-04T21:45:43Z</updated>
		
		<summary type="html">&lt;p&gt;removed &lt;a href=&quot;/wiki/Category:Georgia_State_Route_135&quot; title=&quot;Category:Georgia State Route 135&quot;&gt;Category:Georgia State Route 135&lt;/a&gt;; added &lt;a href=&quot;/wiki/Category:Georgia_State_Route_135_in_Jeff_Davis_County,_Georgia&quot; title=&quot;Category:Georgia State Route 135 in Jeff Davis County, Georgia&quot;&gt;Category:Georgia State Route 135 in Jeff Davis County, Georgia&lt;/a&gt; using &lt;a href=&quot;/wiki/Help:Gadget-HotCat&quot; title=&quot;Help:Gadget-HotCat&quot;&gt;HotCat&lt;/a&gt;&lt;/p&gt;
&lt;table class=&quot;diff diff-contentalign-left&quot; data-mw=&quot;interface&quot;&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;tr style='vertical-align: top;' lang='en'&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;← Older revision&lt;/td&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;Revision as of 21:45, 4 March 2017&lt;/td&gt;
				&lt;/tr&gt;&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 15:&lt;/td&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 15:&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Buildings in Jeff Davis County, Georgia]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Buildings in Jeff Davis County, Georgia]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:December 2015 in Georgia (U.S. state)]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:December 2015 in Georgia (U.S. state)]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;−&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #ffe49c; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Georgia State Route 135]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;+&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #a3d3ff; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Georgia State Route 135&lt;ins class=&quot;diffchange diffchange-inline&quot;&gt; in Jeff Davis County, Georgia&lt;/ins&gt;]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Photographs taken on 2015-12-12|Hazlehurst, Georgia]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Photographs taken on 2015-12-12|Hazlehurst, Georgia]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Pictures by Mjrmtg]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Pictures by Mjrmtg]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;</summary>
		<author><name>Mjrmtg</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:Arkhangelskoye,_Moskovskaya_oblast%27,_Russia_-_panoramio_(39).jpg&amp;diff=236000058&amp;oldid=0</id>
		<title>File:Arkhangelskoye, Moskovskaya oblast', Russia - panoramio (39).jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:Arkhangelskoye,_Moskovskaya_oblast%27,_Russia_-_panoramio_(39).jpg&amp;diff=236000058&amp;oldid=0"/>
				<updated>2017-03-04T21:45:42Z</updated>
		
		<summary type="html">&lt;p&gt;&lt;a href=&quot;/wiki/User:Panoramio_upload_bot&quot; class=&quot;mw-userlink&quot; title=&quot;User:Panoramio upload bot&quot;&gt;&lt;bdi&gt;Panoramio upload bot&lt;/bdi&gt;&lt;/a&gt; uploaded &lt;a href=&quot;/wiki/File:Arkhangelskoye,_Moskovskaya_oblast%27,_Russia_-_panoramio_(39).jpg&quot; title=&quot;File:Arkhangelskoye, Moskovskaya oblast', Russia - panoramio (39).jpg&quot;&gt;File:Arkhangelskoye, Moskovskaya oblast&amp;#039;, Russia - panoramio (39).jpg&lt;/a&gt; == {{int:filedesc}} == {{Information |description=Arkhangelskoye, Moskovskaya oblast&amp;#039;, Russia |date={{Taken on|2012-01-05}} |source=http://www.panoramio.com/photo/94540423 |author=[http://www.panoramio.com/user/1275365?with_photo_id=94540423 karel291]...&lt;/p&gt;
&lt;p&gt;&lt;b&gt;New page&lt;/b&gt;&lt;/p&gt;&lt;div&gt;== {{int:filedesc}} ==&lt;br /&gt;
{{Information&lt;br /&gt;
|description=Arkhangelskoye, Moskovskaya oblast', Russia&lt;br /&gt;
|date={{Taken on|2012-01-05}}&lt;br /&gt;
|source=http://www.panoramio.com/photo/94540423&lt;br /&gt;
|author=[http://www.panoramio.com/user/1275365?with_photo_id=94540423 karel291]&lt;br /&gt;
|permission={{cc-by-3.0|karel291}}&lt;br /&gt;
{{Panoramioreview|Panoramio_upload_bot|2017-03-04}}&lt;br /&gt;
|other_versions=&lt;br /&gt;
|other_fields={{Information field|Name=Tags&amp;lt;br /&amp;gt;(from Panoramio photo page)|Value=&amp;lt;code&amp;gt;Arkhangelskoye&amp;lt;/code&amp;gt;}}&lt;br /&gt;
}}&lt;br /&gt;
{{Location|55.784561|37.283258|source:Panoramio}}&lt;br /&gt;
&lt;br /&gt;
{{Uncategorized|year=2017|month=March|day=4|geo=1}}&lt;br /&gt;
[[Category:Panoramio files uploaded by Panoramio_upload_bot]]&lt;/div&gt;</summary>
		<author><name>Panoramio upload bot</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:Philips_Hall_WCU_2.JPG&amp;diff=236000057&amp;oldid=152255745</id>
		<title>File:Philips Hall WCU 2.JPG</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:Philips_Hall_WCU_2.JPG&amp;diff=236000057&amp;oldid=152255745"/>
				<updated>2017-03-04T21:45:42Z</updated>
		
		<summary type="html">&lt;p&gt;- 3 categories; +&lt;a href=&quot;/w/index.php?title=Category:Philips_Hall_(West_Chester_University_of_Pennsylvania)&amp;amp;action=edit&amp;amp;redlink=1&quot; class=&quot;new&quot; title=&quot;Category:Philips Hall (West Chester University of Pennsylvania) (page does not exist)&quot;&gt;Category:Philips Hall (West Chester University of Pennsylvania)&lt;/a&gt; using &lt;a href=&quot;/wiki/Help:Gadget-HotCat&quot; title=&quot;Help:Gadget-HotCat&quot;&gt;HotCat&lt;/a&gt;&lt;/p&gt;
&lt;table class=&quot;diff diff-contentalign-left&quot; data-mw=&quot;interface&quot;&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;tr style='vertical-align: top;' lang='en'&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;← Older revision&lt;/td&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;Revision as of 21:45, 4 March 2017&lt;/td&gt;
				&lt;/tr&gt;&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 8:&lt;/td&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 8:&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;|other_versions =&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;|other_versions =&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;}}&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;}}&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;−&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #ffe49c; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;&lt;del class=&quot;diffchange diffchange-inline&quot;&gt; &lt;/del&gt;{{Location|39|57|8|N|75|35|55|W}}&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;+&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #a3d3ff; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;{{Location|39|57|8|N|75|35|55|W}}&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;== {{int:license-header}} ==&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;== {{int:license-header}} ==&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;{{PD-self}}&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;{{PD-self}}&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;−&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #ffe49c; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:&lt;del class=&quot;diffchange diffchange-inline&quot;&gt;Buildings&lt;/del&gt; &lt;del class=&quot;diffchange diffchange-inline&quot;&gt;in&lt;/del&gt; West Chester&lt;del class=&quot;diffchange diffchange-inline&quot;&gt;,&lt;/del&gt; Pennsylvania]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;+&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #a3d3ff; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:&lt;ins class=&quot;diffchange diffchange-inline&quot;&gt;Philips&lt;/ins&gt; &lt;ins class=&quot;diffchange diffchange-inline&quot;&gt;Hall&lt;/ins&gt; &lt;ins class=&quot;diffchange diffchange-inline&quot;&gt;(&lt;/ins&gt;West Chester&lt;ins class=&quot;diffchange diffchange-inline&quot;&gt; University of&lt;/ins&gt; Pennsylvania&lt;ins class=&quot;diffchange diffchange-inline&quot;&gt;)&lt;/ins&gt;]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;−&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #ffe49c; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:National Register of Historic Places in Chester County, Pennsylvania]]&lt;/div&gt;&lt;/td&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-empty&quot;&gt;&amp;#160;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;−&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #ffe49c; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:West Chester University of Pennsylvania]]&lt;/div&gt;&lt;/td&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-empty&quot;&gt;&amp;#160;&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;</summary>
		<author><name>Kennethaw88</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=Gem%C3%A4ldegalerie_Alte_Meister_(Dresden),_Galeriewerk_Heineken&amp;diff=236000056&amp;oldid=235990289</id>
		<title>Gemäldegalerie Alte Meister (Dresden), Galeriewerk Heineken</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=Gem%C3%A4ldegalerie_Alte_Meister_(Dresden),_Galeriewerk_Heineken&amp;diff=236000056&amp;oldid=235990289"/>
				<updated>2017-03-04T21:45:40Z</updated>
		
		<summary type="html">&lt;p&gt;‎&lt;span dir=&quot;auto&quot;&gt;&lt;span class=&quot;autocomment&quot;&gt;I. VOLUME. Contenant cinquante pieces avec une description de chaque tableau en françois et en italien.: &lt;/span&gt; ergänzt&lt;/span&gt;&lt;/p&gt;
&lt;table class=&quot;diff diff-contentalign-left&quot; data-mw=&quot;interface&quot;&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;tr style='vertical-align: top;' lang='en'&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;← Older revision&lt;/td&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;Revision as of 21:45, 4 March 2017&lt;/td&gt;
				&lt;/tr&gt;&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 367:&lt;/td&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 367:&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;| align=center | [http://skd-online-collection.skd.museum/de/contents/showSearch?id=354368 354368]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;| align=center | [http://skd-online-collection.skd.museum/de/contents/showSearch?id=354368 354368]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;| Girolamo Muziano&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;| Girolamo Muziano&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;−&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #ffe49c; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;|&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;+&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #a3d3ff; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;|&lt;ins class=&quot;diffchange diffchange-inline&quot;&gt; Francesco Vanni&lt;/ins&gt;&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;| Die Heilige Familie&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;| Die Heilige Familie&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;| Marcello Bacciarelli&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;| Marcello Bacciarelli&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;</summary>
		<author><name>A. Wagner</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:US_Secret_Service_Uniformed_Division_Crown_Victoria_(17218859425).jpg&amp;diff=236000055&amp;oldid=0</id>
		<title>File:US Secret Service Uniformed Division Crown Victoria (17218859425).jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:US_Secret_Service_Uniformed_Division_Crown_Victoria_(17218859425).jpg&amp;diff=236000055&amp;oldid=0"/>
				<updated>2017-03-04T21:45:40Z</updated>
		
		<summary type="html">&lt;p&gt;&lt;a href=&quot;/wiki/User:XXN&quot; class=&quot;mw-userlink&quot; title=&quot;User:XXN&quot;&gt;&lt;bdi&gt;XXN&lt;/bdi&gt;&lt;/a&gt; uploaded &lt;a href=&quot;/wiki/File:US_Secret_Service_Uniformed_Division_Crown_Victoria_(17218859425).jpg&quot; title=&quot;File:US Secret Service Uniformed Division Crown Victoria (17218859425).jpg&quot;&gt;File:US Secret Service Uniformed Division Crown Victoria (17218859425).jpg&lt;/a&gt; Transferred from Flickr via &lt;a href=&quot;/wiki/Commons:Flickr2Commons&quot; title=&quot;Commons:Flickr2Commons&quot;&gt;Flickr2Commons&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;New page&lt;/b&gt;&lt;/p&gt;&lt;div&gt;== {{int:filedesc}} ==&lt;br /&gt;
{{Information&lt;br /&gt;
| Description = US Secret Service Uniformed Division Crown Victoria&lt;br /&gt;
| Source      = [https://www.flickr.com/photos/_schmal/17218859425/ US Secret Service Uniformed Division Crown Victoria]&lt;br /&gt;
| Date        = 2015-04-17 09:49&lt;br /&gt;
| Author      = [https://www.flickr.com/people/132192110@N04 Alex Smith] from Fort Collins, CO, United States&lt;br /&gt;
| Permission  = &lt;br /&gt;
| other_versions=&lt;br /&gt;
}}&lt;br /&gt;
&lt;br /&gt;
=={{int:license-header}}==&lt;br /&gt;
{{cc-zero}}&lt;br /&gt;
{{flickrreview}}&lt;br /&gt;
[[Category:Photographs by Alex Smith]]&lt;/div&gt;</summary>
		<author><name>XXN</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:US_Secret_Service_Uniformed_Division_Ford_Taurus-Police_Interceptor_(17218299671).jpg&amp;diff=236000053&amp;oldid=0</id>
		<title>File:US Secret Service Uniformed Division Ford Taurus-Police Interceptor (17218299671).jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:US_Secret_Service_Uniformed_Division_Ford_Taurus-Police_Interceptor_(17218299671).jpg&amp;diff=236000053&amp;oldid=0"/>
				<updated>2017-03-04T21:45:40Z</updated>
		
		<summary type="html">&lt;p&gt;&lt;a href=&quot;/wiki/User:XXN&quot; class=&quot;mw-userlink&quot; title=&quot;User:XXN&quot;&gt;&lt;bdi&gt;XXN&lt;/bdi&gt;&lt;/a&gt; uploaded &lt;a href=&quot;/wiki/File:US_Secret_Service_Uniformed_Division_Ford_Taurus-Police_Interceptor_(17218299671).jpg&quot; title=&quot;File:US Secret Service Uniformed Division Ford Taurus-Police Interceptor (17218299671).jpg&quot;&gt;File:US Secret Service Uniformed Division Ford Taurus-Police Interceptor (17218299671).jpg&lt;/a&gt; Transferred from Flickr via &lt;a href=&quot;/wiki/Commons:Flickr2Commons&quot; title=&quot;Commons:Flickr2Commons&quot;&gt;Flickr2Commons&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;New page&lt;/b&gt;&lt;/p&gt;&lt;div&gt;== {{int:filedesc}} ==&lt;br /&gt;
{{Information&lt;br /&gt;
| Description = US Secret Service Uniformed Division Ford Taurus/Police Interceptor&lt;br /&gt;
| Source      = [https://www.flickr.com/photos/_schmal/17218299671/ US Secret Service Uniformed Division Ford Taurus/Police Interceptor]&lt;br /&gt;
| Date        = 2015-04-17 09:44&lt;br /&gt;
| Author      = [https://www.flickr.com/people/132192110@N04 Alex Smith] from Fort Collins, CO, United States&lt;br /&gt;
| Permission  = &lt;br /&gt;
| other_versions=&lt;br /&gt;
}}&lt;br /&gt;
&lt;br /&gt;
=={{int:license-header}}==&lt;br /&gt;
{{cc-zero}}&lt;br /&gt;
{{flickrreview}}&lt;br /&gt;
[[Category:Photographs by Alex Smith]]&lt;/div&gt;</summary>
		<author><name>XXN</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:Bellingham,_WA_Police_Ford_Police_Utility_(9091)_(17213837756).jpg&amp;diff=236000052&amp;oldid=0</id>
		<title>File:Bellingham, WA Police Ford Police Utility (9091) (17213837756).jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:Bellingham,_WA_Police_Ford_Police_Utility_(9091)_(17213837756).jpg&amp;diff=236000052&amp;oldid=0"/>
				<updated>2017-03-04T21:45:39Z</updated>
		
		<summary type="html">&lt;p&gt;&lt;a href=&quot;/wiki/User:XXN&quot; class=&quot;mw-userlink&quot; title=&quot;User:XXN&quot;&gt;&lt;bdi&gt;XXN&lt;/bdi&gt;&lt;/a&gt; uploaded &lt;a href=&quot;/wiki/File:Bellingham,_WA_Police_Ford_Police_Utility_(9091)_(17213837756).jpg&quot; title=&quot;File:Bellingham, WA Police Ford Police Utility (9091) (17213837756).jpg&quot;&gt;File:Bellingham, WA Police Ford Police Utility (9091) (17213837756).jpg&lt;/a&gt; Transferred from Flickr via &lt;a href=&quot;/wiki/Commons:Flickr2Commons&quot; title=&quot;Commons:Flickr2Commons&quot;&gt;Flickr2Commons&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;New page&lt;/b&gt;&lt;/p&gt;&lt;div&gt;== {{int:filedesc}} ==&lt;br /&gt;
{{Information&lt;br /&gt;
| Description = I took a few more shots of their new Utilities to give a better all-around view. These vehicles were introduced in January 2015.&lt;br /&gt;
| Source      = [https://www.flickr.com/photos/_schmal/17213837756/ Bellingham, WA Police Ford Police Utility (9091)]&lt;br /&gt;
| Date        = 2015-04-22 12:04&lt;br /&gt;
| Author      = [https://www.flickr.com/people/132192110@N04 Alex Smith] from Fort Collins, CO, United States&lt;br /&gt;
| Permission  = &lt;br /&gt;
| other_versions=&lt;br /&gt;
}}&lt;br /&gt;
&lt;br /&gt;
=={{int:license-header}}==&lt;br /&gt;
{{cc-zero}}&lt;br /&gt;
{{flickrreview}}&lt;br /&gt;
[[Category:Photographs by Alex Smith]]&lt;/div&gt;</summary>
		<author><name>XXN</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=Category:Kyn%C5%A1perk_nad_Oh%C5%99%C3%AD_(train_station)&amp;diff=236000051&amp;oldid=236000007</id>
		<title>Category:Kynšperk nad Ohří (train station)</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=Category:Kyn%C5%A1perk_nad_Oh%C5%99%C3%AD_(train_station)&amp;diff=236000051&amp;oldid=236000007"/>
				<updated>2017-03-04T21:45:37Z</updated>
		
		<summary type="html">&lt;p&gt;added &lt;a href=&quot;/wiki/Category:Train_stations_in_Sokolov_District&quot; title=&quot;Category:Train stations in Sokolov District&quot;&gt;Category:Train stations in Sokolov District&lt;/a&gt; using &lt;a href=&quot;/wiki/Help:Gadget-HotCat&quot; title=&quot;Help:Gadget-HotCat&quot;&gt;HotCat&lt;/a&gt;&lt;/p&gt;
&lt;table class=&quot;diff diff-contentalign-left&quot; data-mw=&quot;interface&quot;&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;tr style='vertical-align: top;' lang='en'&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;← Older revision&lt;/td&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;Revision as of 21:45, 4 March 2017&lt;/td&gt;
				&lt;/tr&gt;&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 1:&lt;/td&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 1:&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Railway line 140 (Czech Republic)]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Railway line 140 (Czech Republic)]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Kynšperk nad Ohří]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Kynšperk nad Ohří]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-empty&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;+&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #a3d3ff; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Train stations in Sokolov District]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;

&lt;!-- diff cache key commonswiki:diff:version:1.11a:oldid:236000007:newid:236000051 --&gt;
&lt;/table&gt;</summary>
		<author><name>Palickap</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:US_Secret_Service_Uniformed_Division_Ford_Police_Interceptor_(17031092888).jpg&amp;diff=236000047&amp;oldid=0</id>
		<title>File:US Secret Service Uniformed Division Ford Police Interceptor (17031092888).jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:US_Secret_Service_Uniformed_Division_Ford_Police_Interceptor_(17031092888).jpg&amp;diff=236000047&amp;oldid=0"/>
				<updated>2017-03-04T21:45:34Z</updated>
		
		<summary type="html">&lt;p&gt;&lt;a href=&quot;/wiki/User:XXN&quot; class=&quot;mw-userlink&quot; title=&quot;User:XXN&quot;&gt;&lt;bdi&gt;XXN&lt;/bdi&gt;&lt;/a&gt; uploaded &lt;a href=&quot;/wiki/File:US_Secret_Service_Uniformed_Division_Ford_Police_Interceptor_(17031092888).jpg&quot; title=&quot;File:US Secret Service Uniformed Division Ford Police Interceptor (17031092888).jpg&quot;&gt;File:US Secret Service Uniformed Division Ford Police Interceptor (17031092888).jpg&lt;/a&gt; Transferred from Flickr via &lt;a href=&quot;/wiki/Commons:Flickr2Commons&quot; title=&quot;Commons:Flickr2Commons&quot;&gt;Flickr2Commons&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;New page&lt;/b&gt;&lt;/p&gt;&lt;div&gt;== {{int:filedesc}} ==&lt;br /&gt;
{{Information&lt;br /&gt;
| Description = US Secret Service Uniformed Division Ford Police Interceptor&lt;br /&gt;
| Source      = [https://www.flickr.com/photos/_schmal/17031092888/ US Secret Service Uniformed Division Ford Police Interceptor]&lt;br /&gt;
| Date        = 2015-04-17 09:45&lt;br /&gt;
| Author      = [https://www.flickr.com/people/132192110@N04 Alex Smith] from Fort Collins, CO, United States&lt;br /&gt;
| Permission  = &lt;br /&gt;
| other_versions=&lt;br /&gt;
}}&lt;br /&gt;
&lt;br /&gt;
=={{int:license-header}}==&lt;br /&gt;
{{cc-zero}}&lt;br /&gt;
{{flickrreview}}&lt;br /&gt;
[[Category:Photographs by Alex Smith]]&lt;/div&gt;</summary>
		<author><name>XXN</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:Mount_Pleasant,_Pennsylvania_(8483201056).jpg&amp;diff=236000048&amp;oldid=94463358</id>
		<title>File:Mount Pleasant, Pennsylvania (8483201056).jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:Mount_Pleasant,_Pennsylvania_(8483201056).jpg&amp;diff=236000048&amp;oldid=94463358"/>
				<updated>2017-03-04T21:45:34Z</updated>
		
		<summary type="html">&lt;p&gt;+&lt;a href=&quot;/wiki/Category:Churches_in_Westmoreland_County,_Pennsylvania&quot; title=&quot;Category:Churches in Westmoreland County, Pennsylvania&quot;&gt;Category:Churches in Westmoreland County, Pennsylvania&lt;/a&gt;; +&lt;a href=&quot;/wiki/Category:Churches_in_the_United_States_photographed_in_2012&quot; title=&quot;Category:Churches in the United States photographed in 2012&quot;&gt;Category:Churches in the United States photographed in 2012&lt;/a&gt; using &lt;a href=&quot;/wiki/Help:Gadget-HotCat&quot; title=&quot;Help:Gadget-HotCat&quot;&gt;HotCat&lt;/a&gt;&lt;/p&gt;
&lt;table class=&quot;diff diff-contentalign-left&quot; data-mw=&quot;interface&quot;&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;tr style='vertical-align: top;' lang='en'&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;← Older revision&lt;/td&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;Revision as of 21:45, 4 March 2017&lt;/td&gt;
				&lt;/tr&gt;&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 14:&lt;/td&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 14:&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Mount Pleasant, Pennsylvania]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Mount Pleasant, Pennsylvania]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Photographs by Doug Kerr]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Photographs by Doug Kerr]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-empty&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;+&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #a3d3ff; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Churches in Westmoreland County, Pennsylvania]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-empty&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;+&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #a3d3ff; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Churches in the United States photographed in 2012]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;

&lt;!-- diff cache key commonswiki:diff:version:1.11a:oldid:94463358:newid:236000048 --&gt;
&lt;/table&gt;</summary>
		<author><name>Farragutful</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:Zorn_Haus_-_panoramio.jpg&amp;diff=236000046&amp;oldid=233019862</id>
		<title>File:Zorn Haus - panoramio.jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:Zorn_Haus_-_panoramio.jpg&amp;diff=236000046&amp;oldid=233019862"/>
				<updated>2017-03-04T21:45:34Z</updated>
		
		<summary type="html">&lt;p&gt;&lt;/p&gt;
&lt;table class=&quot;diff diff-contentalign-left&quot; data-mw=&quot;interface&quot;&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;tr style='vertical-align: top;' lang='en'&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;← Older revision&lt;/td&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;Revision as of 21:45, 4 March 2017&lt;/td&gt;
				&lt;/tr&gt;&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 12:&lt;/td&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 12:&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;{{Location|47.726205|10.315467|source:Panoramio}}&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;{{Location|47.726205|10.315467|source:Panoramio}}&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;−&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #ffe49c; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;&lt;del class=&quot;diffchange diffchange-inline&quot;&gt;{{Uncategorized|year=2017|month=February|day=10|geo=1}}&lt;/del&gt;&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;+&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #a3d3ff; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;&lt;ins class=&quot;diffchange diffchange-inline&quot;&gt;[[Category:Commerce buildings in Kempten (Allgäu)]]&lt;/ins&gt;&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Panoramio files uploaded by Panoramio_upload_bot]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Panoramio files uploaded by Panoramio_upload_bot]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;

&lt;!-- diff cache key commonswiki:diff:version:1.11a:oldid:233019862:newid:236000046 --&gt;
&lt;/table&gt;</summary>
		<author><name>78.42.162.196</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:Larimer_County_Sheriff%27s_Jeep_Wrangler_(18063775801).jpg&amp;diff=236000044&amp;oldid=0</id>
		<title>File:Larimer County Sheriff's Jeep Wrangler (18063775801).jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:Larimer_County_Sheriff%27s_Jeep_Wrangler_(18063775801).jpg&amp;diff=236000044&amp;oldid=0"/>
				<updated>2017-03-04T21:45:32Z</updated>
		
		<summary type="html">&lt;p&gt;&lt;a href=&quot;/wiki/User:XXN&quot; class=&quot;mw-userlink&quot; title=&quot;User:XXN&quot;&gt;&lt;bdi&gt;XXN&lt;/bdi&gt;&lt;/a&gt; uploaded &lt;a href=&quot;/wiki/File:Larimer_County_Sheriff%27s_Jeep_Wrangler_(18063775801).jpg&quot; title=&quot;File:Larimer County Sheriff's Jeep Wrangler (18063775801).jpg&quot;&gt;File:Larimer County Sheriff&amp;#039;s Jeep Wrangler (18063775801).jpg&lt;/a&gt; Transferred from Flickr via &lt;a href=&quot;/wiki/Commons:Flickr2Commons&quot; title=&quot;Commons:Flickr2Commons&quot;&gt;Flickr2Commons&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;New page&lt;/b&gt;&lt;/p&gt;&lt;div&gt;== {{int:filedesc}} ==&lt;br /&gt;
{{Information&lt;br /&gt;
| Description = The Jeep Wrangler used by the Larimer County Sheriff.&lt;br /&gt;
| Source      = [https://www.flickr.com/photos/_schmal/18063775801/ Larimer County Sheriff's  Jeep Wrangler]&lt;br /&gt;
| Date        = 2015-05-24 16:15&lt;br /&gt;
| Author      = [https://www.flickr.com/people/132192110@N04 Alex Smith] from Fort Collins, CO, United States&lt;br /&gt;
| Permission  = &lt;br /&gt;
| other_versions=&lt;br /&gt;
}}&lt;br /&gt;
&lt;br /&gt;
=={{int:license-header}}==&lt;br /&gt;
{{cc-zero}}&lt;br /&gt;
{{flickrreview}}&lt;br /&gt;
&lt;br /&gt;
[[Category:Jeep Wrangler]]&lt;br /&gt;
[[Category:Photographs by Alex Smith]]&lt;/div&gt;</summary>
		<author><name>XXN</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:Washington,_DC_Fire-EMS_Ambulance_(17217170962).jpg&amp;diff=236000041&amp;oldid=0</id>
		<title>File:Washington, DC Fire-EMS Ambulance (17217170962).jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:Washington,_DC_Fire-EMS_Ambulance_(17217170962).jpg&amp;diff=236000041&amp;oldid=0"/>
				<updated>2017-03-04T21:45:29Z</updated>
		
		<summary type="html">&lt;p&gt;&lt;a href=&quot;/wiki/User:XXN&quot; class=&quot;mw-userlink&quot; title=&quot;User:XXN&quot;&gt;&lt;bdi&gt;XXN&lt;/bdi&gt;&lt;/a&gt; uploaded &lt;a href=&quot;/wiki/File:Washington,_DC_Fire-EMS_Ambulance_(17217170962).jpg&quot; title=&quot;File:Washington, DC Fire-EMS Ambulance (17217170962).jpg&quot;&gt;File:Washington, DC Fire-EMS Ambulance (17217170962).jpg&lt;/a&gt; Transferred from Flickr via &lt;a href=&quot;/wiki/Commons:Flickr2Commons&quot; title=&quot;Commons:Flickr2Commons&quot;&gt;Flickr2Commons&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;New page&lt;/b&gt;&lt;/p&gt;&lt;div&gt;== {{int:filedesc}} ==&lt;br /&gt;
{{Information&lt;br /&gt;
| Description = Washington, DC Fire/EMS Ambulance&lt;br /&gt;
| Source      = [https://www.flickr.com/photos/_schmal/17217170962/ Washington, DC Fire/EMS Ambulance]&lt;br /&gt;
| Date        = 2015-04-17 10:37&lt;br /&gt;
| Author      = [https://www.flickr.com/people/132192110@N04 Alex Smith] from Fort Collins, CO, United States&lt;br /&gt;
| Permission  = &lt;br /&gt;
| other_versions=&lt;br /&gt;
}}&lt;br /&gt;
&lt;br /&gt;
=={{int:license-header}}==&lt;br /&gt;
{{cc-zero}}&lt;br /&gt;
{{flickrreview}}&lt;br /&gt;
[[Category:Photographs by Alex Smith]]&lt;/div&gt;</summary>
		<author><name>XXN</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:Washington,_DC_Metro_Police_Ford_Police_Interceptor_(17011439647).jpg&amp;diff=236000042&amp;oldid=0</id>
		<title>File:Washington, DC Metro Police Ford Police Interceptor (17011439647).jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:Washington,_DC_Metro_Police_Ford_Police_Interceptor_(17011439647).jpg&amp;diff=236000042&amp;oldid=0"/>
				<updated>2017-03-04T21:45:29Z</updated>
		
		<summary type="html">&lt;p&gt;&lt;a href=&quot;/wiki/User:XXN&quot; class=&quot;mw-userlink&quot; title=&quot;User:XXN&quot;&gt;&lt;bdi&gt;XXN&lt;/bdi&gt;&lt;/a&gt; uploaded &lt;a href=&quot;/wiki/File:Washington,_DC_Metro_Police_Ford_Police_Interceptor_(17011439647).jpg&quot; title=&quot;File:Washington, DC Metro Police Ford Police Interceptor (17011439647).jpg&quot;&gt;File:Washington, DC Metro Police Ford Police Interceptor (17011439647).jpg&lt;/a&gt; Transferred from Flickr via &lt;a href=&quot;/wiki/Commons:Flickr2Commons&quot; title=&quot;Commons:Flickr2Commons&quot;&gt;Flickr2Commons&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;New page&lt;/b&gt;&lt;/p&gt;&lt;div&gt;== {{int:filedesc}} ==&lt;br /&gt;
{{Information&lt;br /&gt;
| Description = Washington, DC Metro Police Ford Police Interceptor&lt;br /&gt;
| Source      = [https://www.flickr.com/photos/_schmal/17011439647/ Washington, DC Metro Police Ford Police Interceptor]&lt;br /&gt;
| Date        = 2015-04-17 14:57&lt;br /&gt;
| Author      = [https://www.flickr.com/people/132192110@N04 Alex Smith] from Fort Collins, CO, United States&lt;br /&gt;
| Permission  = &lt;br /&gt;
| other_versions=&lt;br /&gt;
}}&lt;br /&gt;
&lt;br /&gt;
=={{int:license-header}}==&lt;br /&gt;
{{cc-zero}}&lt;br /&gt;
{{flickrreview}}&lt;br /&gt;
[[Category:Photographs by Alex Smith]]&lt;/div&gt;</summary>
		<author><name>XXN</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:Washington,_DC_Metro_Police_Ford_Explorer-Utility_(17031093078).jpg&amp;diff=236000040&amp;oldid=0</id>
		<title>File:Washington, DC Metro Police Ford Explorer-Utility (17031093078).jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:Washington,_DC_Metro_Police_Ford_Explorer-Utility_(17031093078).jpg&amp;diff=236000040&amp;oldid=0"/>
				<updated>2017-03-04T21:45:29Z</updated>
		
		<summary type="html">&lt;p&gt;&lt;a href=&quot;/wiki/User:XXN&quot; class=&quot;mw-userlink&quot; title=&quot;User:XXN&quot;&gt;&lt;bdi&gt;XXN&lt;/bdi&gt;&lt;/a&gt; uploaded &lt;a href=&quot;/wiki/File:Washington,_DC_Metro_Police_Ford_Explorer-Utility_(17031093078).jpg&quot; title=&quot;File:Washington, DC Metro Police Ford Explorer-Utility (17031093078).jpg&quot;&gt;File:Washington, DC Metro Police Ford Explorer-Utility (17031093078).jpg&lt;/a&gt; Transferred from Flickr via &lt;a href=&quot;/wiki/Commons:Flickr2Commons&quot; title=&quot;Commons:Flickr2Commons&quot;&gt;Flickr2Commons&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;New page&lt;/b&gt;&lt;/p&gt;&lt;div&gt;== {{int:filedesc}} ==&lt;br /&gt;
{{Information&lt;br /&gt;
| Description = Washington, DC Metro Police Ford Explorer/Utility&lt;br /&gt;
| Source      = [https://www.flickr.com/photos/_schmal/17031093078/ Washington, DC Metro Police Ford Explorer/Utility]&lt;br /&gt;
| Date        = 2015-04-17 09:49&lt;br /&gt;
| Author      = [https://www.flickr.com/people/132192110@N04 Alex Smith] from Fort Collins, CO, United States&lt;br /&gt;
| Permission  = &lt;br /&gt;
| other_versions=&lt;br /&gt;
}}&lt;br /&gt;
&lt;br /&gt;
=={{int:license-header}}==&lt;br /&gt;
{{cc-zero}}&lt;br /&gt;
{{flickrreview}}&lt;br /&gt;
[[Category:Photographs by Alex Smith]]&lt;/div&gt;</summary>
		<author><name>XXN</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:KANTOR_KECAMATAN_GALIS_-_panoramio.jpg&amp;diff=236000038&amp;oldid=0</id>
		<title>File:KANTOR KECAMATAN GALIS - panoramio.jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:KANTOR_KECAMATAN_GALIS_-_panoramio.jpg&amp;diff=236000038&amp;oldid=0"/>
				<updated>2017-03-04T21:45:28Z</updated>
		
		<summary type="html">&lt;p&gt;&lt;a href=&quot;/wiki/User:Panoramio_upload_bot&quot; class=&quot;mw-userlink&quot; title=&quot;User:Panoramio upload bot&quot;&gt;&lt;bdi&gt;Panoramio upload bot&lt;/bdi&gt;&lt;/a&gt; uploaded &lt;a href=&quot;/wiki/File:KANTOR_KECAMATAN_GALIS_-_panoramio.jpg&quot; title=&quot;File:KANTOR KECAMATAN GALIS - panoramio.jpg&quot;&gt;File:KANTOR KECAMATAN GALIS - panoramio.jpg&lt;/a&gt; == {{int:filedesc}} == {{Information |description=KANTOR KECAMATAN GALIS |date={{Taken on|2013-08-07}} |source=http://www.panoramio.com/photo/94315928 |author=[http://www.panoramio.com/user/4346737?with_photo_id=94315928 Syafii muhammad muni…] |permi...&lt;/p&gt;
&lt;p&gt;&lt;b&gt;New page&lt;/b&gt;&lt;/p&gt;&lt;div&gt;== {{int:filedesc}} ==&lt;br /&gt;
{{Information&lt;br /&gt;
|description=KANTOR KECAMATAN GALIS&lt;br /&gt;
|date={{Taken on|2013-08-07}}&lt;br /&gt;
|source=http://www.panoramio.com/photo/94315928&lt;br /&gt;
|author=[http://www.panoramio.com/user/4346737?with_photo_id=94315928 Syafii muhammad muni…]&lt;br /&gt;
|permission={{cc-by-sa-3.0|Syafii muhammad muni…}}&lt;br /&gt;
{{Panoramioreview|Panoramio_upload_bot|2017-03-04}}&lt;br /&gt;
|other_versions=&lt;br /&gt;
|other_fields={{Information field|Name=Tags&amp;lt;br /&amp;gt;(from Panoramio photo page)|Value=&amp;lt;code&amp;gt;Galis&amp;lt;/code&amp;gt;}}&lt;br /&gt;
}}&lt;br /&gt;
{{Location|-7.112631|112.962722|source:Panoramio}}&lt;br /&gt;
&lt;br /&gt;
{{Check categories|year=2017|month=March|day=4}}&lt;br /&gt;
&lt;br /&gt;
[[Category:Galis]]&lt;br /&gt;
[[Category:Panoramio files uploaded by Panoramio_upload_bot]]&lt;/div&gt;</summary>
		<author><name>Panoramio upload bot</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=Category:Tr%C3%B6lladyngja_(North_Iceland)&amp;diff=236000037&amp;oldid=113280426</id>
		<title>Category:Trölladyngja (North Iceland)</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=Category:Tr%C3%B6lladyngja_(North_Iceland)&amp;diff=236000037&amp;oldid=113280426"/>
				<updated>2017-03-04T21:45:28Z</updated>
		
		<summary type="html">&lt;p&gt;removed &lt;a href=&quot;/wiki/Category:Volcanoes_of_Iceland&quot; title=&quot;Category:Volcanoes of Iceland&quot;&gt;Category:Volcanoes of Iceland&lt;/a&gt;; added &lt;a href=&quot;/wiki/Category:Volcanoes_of_Iceland_by_name&quot; title=&quot;Category:Volcanoes of Iceland by name&quot;&gt;Category:Volcanoes of Iceland by name&lt;/a&gt; using &lt;a href=&quot;/wiki/Help:Gadget-HotCat&quot; title=&quot;Help:Gadget-HotCat&quot;&gt;HotCat&lt;/a&gt;&lt;/p&gt;
&lt;table class=&quot;diff diff-contentalign-left&quot; data-mw=&quot;interface&quot;&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;tr style='vertical-align: top;' lang='en'&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;← Older revision&lt;/td&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;Revision as of 21:45, 4 March 2017&lt;/td&gt;
				&lt;/tr&gt;&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 1:&lt;/td&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 1:&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Shield volcanoes]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Shield volcanoes]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:North-east-Iceland]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:North-east-Iceland]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;−&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #ffe49c; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Volcanoes of Iceland]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;+&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #a3d3ff; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Volcanoes of Iceland&lt;ins class=&quot;diffchange diffchange-inline&quot;&gt; by name&lt;/ins&gt;]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Shield volcanoes of Iceland]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Shield volcanoes of Iceland]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;

&lt;!-- diff cache key commonswiki:diff:version:1.11a:oldid:113280426:newid:236000037 --&gt;
&lt;/table&gt;</summary>
		<author><name>Reykholt</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=Category:Poor_Clares_convents_by_country&amp;diff=236000034&amp;oldid=0</id>
		<title>Category:Poor Clares convents by country</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=Category:Poor_Clares_convents_by_country&amp;diff=236000034&amp;oldid=0"/>
				<updated>2017-03-04T21:45:27Z</updated>
		
		<summary type="html">&lt;p&gt;N&lt;/p&gt;
&lt;p&gt;&lt;b&gt;New page&lt;/b&gt;&lt;/p&gt;&lt;div&gt;[[Category:Poor Clares convents| ]]&lt;br /&gt;
[[Category:Convents by country by fraternity]]&lt;/div&gt;</summary>
		<author><name>Janezdrilc</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:Trenino_verde,_tratta_Sassari-Palau_(30).jpg&amp;diff=236000033&amp;oldid=235999900</id>
		<title>File:Trenino verde, tratta Sassari-Palau (30).jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:Trenino_verde,_tratta_Sassari-Palau_(30).jpg&amp;diff=236000033&amp;oldid=235999900"/>
				<updated>2017-03-04T21:45:25Z</updated>
		
		<summary type="html">&lt;p&gt;added &lt;a href=&quot;/wiki/Category:Lago_del_Liscia_train_station&quot; title=&quot;Category:Lago del Liscia train station&quot;&gt;Category:Lago del Liscia train station&lt;/a&gt; using &lt;a href=&quot;/wiki/Help:Gadget-HotCat&quot; title=&quot;Help:Gadget-HotCat&quot;&gt;HotCat&lt;/a&gt;&lt;/p&gt;
&lt;table class=&quot;diff diff-contentalign-left&quot; data-mw=&quot;interface&quot;&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;tr style='vertical-align: top;' lang='en'&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;← Older revision&lt;/td&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;Revision as of 21:45, 4 March 2017&lt;/td&gt;
				&lt;/tr&gt;&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 15:&lt;/td&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 15:&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Trenino Verde della Sardegna]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Trenino Verde della Sardegna]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Photographs by Gianni Careddu]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Photographs by Gianni Careddu]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-empty&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;+&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #a3d3ff; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Lago del Liscia train station]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;

&lt;!-- diff cache key commonswiki:diff:version:1.11a:oldid:235999900:newid:236000033 --&gt;
&lt;/table&gt;</summary>
		<author><name>Discanto</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:NNational_Zoological_Park_Delhiational_Zoological_Park_376.JPG&amp;diff=236000032&amp;oldid=236000014</id>
		<title>File:NNational Zoological Park Delhiational Zoological Park 376.JPG</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:NNational_Zoological_Park_Delhiational_Zoological_Park_376.JPG&amp;diff=236000032&amp;oldid=236000014"/>
				<updated>2017-03-04T21:45:25Z</updated>
		
		<summary type="html">&lt;p&gt;added &lt;a href=&quot;/wiki/Category:Lying_animals&quot; title=&quot;Category:Lying animals&quot;&gt;Category:Lying animals&lt;/a&gt; using &lt;a href=&quot;/wiki/Help:Gadget-HotCat&quot; title=&quot;Help:Gadget-HotCat&quot;&gt;HotCat&lt;/a&gt;&lt;/p&gt;
&lt;table class=&quot;diff diff-contentalign-left&quot; data-mw=&quot;interface&quot;&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;tr style='vertical-align: top;' lang='en'&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;← Older revision&lt;/td&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;Revision as of 21:45, 4 March 2017&lt;/td&gt;
				&lt;/tr&gt;&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 14:&lt;/td&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 14:&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Mammals in National Zoological Park Delhi]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Mammals in National Zoological Park Delhi]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Papio hamadryas in zoos]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Papio hamadryas in zoos]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-empty&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;+&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #a3d3ff; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Lying animals]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;

&lt;!-- diff cache key commonswiki:diff:version:1.11a:oldid:236000014:newid:236000032 --&gt;
&lt;/table&gt;</summary>
		<author><name>Arnaud Palastowicz</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:Museo_Nacional_de_Antropolog%C3%ADa_-_Wiki_takes_Antropolog%C3%ADa_079.jpg&amp;diff=236000031&amp;oldid=205034680</id>
		<title>File:Museo Nacional de Antropología - Wiki takes Antropología 079.jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:Museo_Nacional_de_Antropolog%C3%ADa_-_Wiki_takes_Antropolog%C3%ADa_079.jpg&amp;diff=236000031&amp;oldid=205034680"/>
				<updated>2017-03-04T21:45:24Z</updated>
		
		<summary type="html">&lt;p&gt;&lt;/p&gt;
&lt;table class=&quot;diff diff-contentalign-left&quot; data-mw=&quot;interface&quot;&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;tr style='vertical-align: top;' lang='en'&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;← Older revision&lt;/td&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;Revision as of 21:45, 4 March 2017&lt;/td&gt;
				&lt;/tr&gt;&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 14:&lt;/td&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 14:&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Wiki takes Antropología]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Wiki takes Antropología]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Wikimania 2015 photographs by Mike Peel]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Wikimania 2015 photographs by Mike Peel]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;−&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #ffe49c; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:National Museum of Anthropology]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;+&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #a3d3ff; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:National Museum of Anthropology&lt;ins class=&quot;diffchange diffchange-inline&quot;&gt; - Pakal tomb&lt;/ins&gt;]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;

&lt;!-- diff cache key commonswiki:diff:version:1.11a:oldid:205034680:newid:236000031 --&gt;
&lt;/table&gt;</summary>
		<author><name>Xenophon</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:1902_Evionnaz,_Switzerland_-_panoramio_(1).jpg&amp;diff=236000030&amp;oldid=0</id>
		<title>File:1902 Evionnaz, Switzerland - panoramio (1).jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:1902_Evionnaz,_Switzerland_-_panoramio_(1).jpg&amp;diff=236000030&amp;oldid=0"/>
				<updated>2017-03-04T21:45:23Z</updated>
		
		<summary type="html">&lt;p&gt;&lt;a href=&quot;/wiki/User:Panoramio_upload_bot&quot; class=&quot;mw-userlink&quot; title=&quot;User:Panoramio upload bot&quot;&gt;&lt;bdi&gt;Panoramio upload bot&lt;/bdi&gt;&lt;/a&gt; uploaded &lt;a href=&quot;/wiki/File:1902_Evionnaz,_Switzerland_-_panoramio_(1).jpg&quot; title=&quot;File:1902 Evionnaz, Switzerland - panoramio (1).jpg&quot;&gt;File:1902 Evionnaz, Switzerland - panoramio (1).jpg&lt;/a&gt; == {{int:filedesc}} == {{Information |description=1902 Evionnaz, Switzerland |date={{Taken on|2002-09-02}} |source=http://www.panoramio.com/photo/94540349 |author=[http://www.panoramio.com/user/6004592?with_photo_id=94540349 Björn S.] |permission={{cc...&lt;/p&gt;
&lt;p&gt;&lt;b&gt;New page&lt;/b&gt;&lt;/p&gt;&lt;div&gt;== {{int:filedesc}} ==&lt;br /&gt;
{{Information&lt;br /&gt;
|description=1902 Evionnaz, Switzerland&lt;br /&gt;
|date={{Taken on|2002-09-02}}&lt;br /&gt;
|source=http://www.panoramio.com/photo/94540349&lt;br /&gt;
|author=[http://www.panoramio.com/user/6004592?with_photo_id=94540349 Björn S.]&lt;br /&gt;
|permission={{cc-by-sa-3.0|Björn S.}}&lt;br /&gt;
{{Panoramioreview|Panoramio_upload_bot|2017-03-04}}&lt;br /&gt;
|other_versions=&lt;br /&gt;
|other_fields={{Information field|Name=Tags&amp;lt;br /&amp;gt;(from Panoramio photo page)|Value=&amp;lt;code&amp;gt;Evionnaz&amp;lt;/code&amp;gt;, &amp;lt;code&amp;gt;Dents du  Midi&amp;lt;/code&amp;gt;}}&lt;br /&gt;
}}&lt;br /&gt;
{{Location|46.146916|6.94724|source:Panoramio}}&lt;br /&gt;
&lt;br /&gt;
{{Check categories|year=2017|month=March|day=4}}&lt;br /&gt;
&lt;br /&gt;
[[Category:Evionnaz]]&lt;br /&gt;
[[Category:Dents du  Midi]]&lt;br /&gt;
[[Category:Panoramio files uploaded by Panoramio_upload_bot]]&lt;/div&gt;</summary>
		<author><name>Panoramio upload bot</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:Hazlehurst_City_limit_US221SB.jpg&amp;diff=236000028&amp;oldid=224262473</id>
		<title>File:Hazlehurst City limit US221SB.jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:Hazlehurst_City_limit_US221SB.jpg&amp;diff=236000028&amp;oldid=224262473"/>
				<updated>2017-03-04T21:45:21Z</updated>
		
		<summary type="html">&lt;p&gt;removed &lt;a href=&quot;/wiki/Category:Georgia_State_Route_135&quot; title=&quot;Category:Georgia State Route 135&quot;&gt;Category:Georgia State Route 135&lt;/a&gt;; added &lt;a href=&quot;/wiki/Category:Georgia_State_Route_135_in_Jeff_Davis_County,_Georgia&quot; title=&quot;Category:Georgia State Route 135 in Jeff Davis County, Georgia&quot;&gt;Category:Georgia State Route 135 in Jeff Davis County, Georgia&lt;/a&gt; using &lt;a href=&quot;/wiki/Help:Gadget-HotCat&quot; title=&quot;Help:Gadget-HotCat&quot;&gt;HotCat&lt;/a&gt;&lt;/p&gt;
&lt;table class=&quot;diff diff-contentalign-left&quot; data-mw=&quot;interface&quot;&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;tr style='vertical-align: top;' lang='en'&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;← Older revision&lt;/td&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;Revision as of 21:45, 4 March 2017&lt;/td&gt;
				&lt;/tr&gt;&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 13:&lt;/td&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 13:&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;{{self|cc-by-sa-4.0}}&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;{{self|cc-by-sa-4.0}}&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;−&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #ffe49c; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Georgia State Route 135]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;+&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #a3d3ff; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Georgia State Route 135&lt;ins class=&quot;diffchange diffchange-inline&quot;&gt; in Jeff Davis County, Georgia&lt;/ins&gt;]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Green and white rectangular road signs]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Green and white rectangular road signs]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Hazlehurst, Georgia]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Hazlehurst, Georgia]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;

&lt;!-- diff cache key commonswiki:diff:version:1.11a:oldid:224262473:newid:236000028 --&gt;
&lt;/table&gt;</summary>
		<author><name>Mjrmtg</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:United_States_Capitol_Police_Crown_Victoria_(17192918126).jpg&amp;diff=236000026&amp;oldid=0</id>
		<title>File:United States Capitol Police Crown Victoria (17192918126).jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:United_States_Capitol_Police_Crown_Victoria_(17192918126).jpg&amp;diff=236000026&amp;oldid=0"/>
				<updated>2017-03-04T21:45:20Z</updated>
		
		<summary type="html">&lt;p&gt;&lt;a href=&quot;/wiki/User:XXN&quot; class=&quot;mw-userlink&quot; title=&quot;User:XXN&quot;&gt;&lt;bdi&gt;XXN&lt;/bdi&gt;&lt;/a&gt; uploaded &lt;a href=&quot;/wiki/File:United_States_Capitol_Police_Crown_Victoria_(17192918126).jpg&quot; title=&quot;File:United States Capitol Police Crown Victoria (17192918126).jpg&quot;&gt;File:United States Capitol Police Crown Victoria (17192918126).jpg&lt;/a&gt; Transferred from Flickr via &lt;a href=&quot;/wiki/Commons:Flickr2Commons&quot; title=&quot;Commons:Flickr2Commons&quot;&gt;Flickr2Commons&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;New page&lt;/b&gt;&lt;/p&gt;&lt;div&gt;== {{int:filedesc}} ==&lt;br /&gt;
{{Information&lt;br /&gt;
| Description = United States Capitol Police Crown Victoria&lt;br /&gt;
| Source      = [https://www.flickr.com/photos/_schmal/17192918126/ United States Capitol Police Crown Victoria]&lt;br /&gt;
| Date        = 2015-04-17 14:41&lt;br /&gt;
| Author      = [https://www.flickr.com/people/132192110@N04 Alex Smith] from Fort Collins, CO, United States&lt;br /&gt;
| Permission  = &lt;br /&gt;
| other_versions=&lt;br /&gt;
}}&lt;br /&gt;
&lt;br /&gt;
=={{int:license-header}}==&lt;br /&gt;
{{cc-zero}}&lt;br /&gt;
{{flickrreview}}&lt;br /&gt;
[[Category:Photographs by Alex Smith]]&lt;/div&gt;</summary>
		<author><name>XXN</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:Lefaucheux_M1858.svg&amp;diff=236000025&amp;oldid=126946492</id>
		<title>File:Lefaucheux M1858.svg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:Lefaucheux_M1858.svg&amp;diff=236000025&amp;oldid=126946492"/>
				<updated>2017-03-04T21:45:20Z</updated>
		
		<summary type="html">&lt;p&gt;added &lt;a href=&quot;/wiki/Category:Revolvers_in_art&quot; title=&quot;Category:Revolvers in art&quot;&gt;Category:Revolvers in art&lt;/a&gt; using &lt;a href=&quot;/wiki/Help:Gadget-HotCat&quot; title=&quot;Help:Gadget-HotCat&quot;&gt;HotCat&lt;/a&gt;&lt;/p&gt;
&lt;table class=&quot;diff diff-contentalign-left&quot; data-mw=&quot;interface&quot;&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;tr style='vertical-align: top;' lang='en'&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;← Older revision&lt;/td&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;Revision as of 21:45, 4 March 2017&lt;/td&gt;
				&lt;/tr&gt;&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 14:&lt;/td&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 14:&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Lefaucheux M1858]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Lefaucheux M1858]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:SVG firearms and ammunition]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:SVG firearms and ammunition]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-empty&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;+&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #a3d3ff; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Revolvers in art]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;

&lt;!-- diff cache key commonswiki:diff:version:1.11a:oldid:126946492:newid:236000025 --&gt;
&lt;/table&gt;</summary>
		<author><name>SunOfErat</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:Krynki._%D0%9A%D1%80%D1%8B%D0%BD%D0%BA%D1%96_(I._Sierba%C5%AD,_1911).jpg&amp;diff=236000024&amp;oldid=182886828</id>
		<title>File:Krynki. Крынкі (I. Sierbaŭ, 1911).jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:Krynki._%D0%9A%D1%80%D1%8B%D0%BD%D0%BA%D1%96_(I._Sierba%C5%AD,_1911).jpg&amp;diff=236000024&amp;oldid=182886828"/>
				<updated>2017-03-04T21:45:20Z</updated>
		
		<summary type="html">&lt;p&gt;removed &lt;a href=&quot;/wiki/Category:Asipovi%C4%8Dy_District&quot; title=&quot;Category:Asipovičy District&quot;&gt;Category:Asipovičy District&lt;/a&gt;; added &lt;a href=&quot;/wiki/Category:Krynki,_Asipovichy_District&quot; title=&quot;Category:Krynki, Asipovichy District&quot;&gt;Category:Krynki, Asipovichy District&lt;/a&gt; using &lt;a href=&quot;/wiki/Help:Gadget-HotCat&quot; title=&quot;Help:Gadget-HotCat&quot;&gt;HotCat&lt;/a&gt;&lt;/p&gt;
&lt;table class=&quot;diff diff-contentalign-left&quot; data-mw=&quot;interface&quot;&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;tr style='vertical-align: top;' lang='en'&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;← Older revision&lt;/td&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;Revision as of 21:45, 4 March 2017&lt;/td&gt;
				&lt;/tr&gt;&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 10:&lt;/td&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 10:&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:1911 in Belarus]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:1911 in Belarus]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Isaak Serbov]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Isaak Serbov]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;−&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #ffe49c; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:&lt;del class=&quot;diffchange diffchange-inline&quot;&gt;Asipovičy&lt;/del&gt; District]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;+&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #a3d3ff; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:&lt;ins class=&quot;diffchange diffchange-inline&quot;&gt;Krynki, Asipovichy&lt;/ins&gt; District]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;

&lt;!-- diff cache key commonswiki:diff:version:1.11a:oldid:182886828:newid:236000024 --&gt;
&lt;/table&gt;</summary>
		<author><name>KarlKori</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=Category:Visconti_Bridge_(Pavlovsk)&amp;diff=236000023&amp;oldid=235993042</id>
		<title>Category:Visconti Bridge (Pavlovsk)</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=Category:Visconti_Bridge_(Pavlovsk)&amp;diff=236000023&amp;oldid=235993042"/>
				<updated>2017-03-04T21:45:19Z</updated>
		
		<summary type="html">&lt;p&gt;&lt;/p&gt;
&lt;table class=&quot;diff diff-contentalign-left&quot; data-mw=&quot;interface&quot;&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;tr style='vertical-align: top;' lang='en'&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;← Older revision&lt;/td&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;Revision as of 21:45, 4 March 2017&lt;/td&gt;
				&lt;/tr&gt;&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 1:&lt;/td&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 1:&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-empty&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;+&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #a3d3ff; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;{{Cultural Heritage Russia&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-empty&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;+&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #a3d3ff; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;|id= 7810339051&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-empty&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;+&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #a3d3ff; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;|title=Висконтиев мост-плотина&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-empty&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;+&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #a3d3ff; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;|place=Санкт-Петербург, Павловск, долина р. Славянки&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-empty&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;+&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #a3d3ff; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;|built= 1802-1803 гг., строитель К. Висконти, арх. А.Н. Воронихин&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-empty&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;+&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #a3d3ff; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;|approved=Постановление Правительства РФ № 527 от 10.07.2001&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-empty&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;+&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #a3d3ff; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;|category=Pavlovsk, Saint Petersburg&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-empty&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;+&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #a3d3ff; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;}}&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-empty&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;+&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #a3d3ff; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Bridges in Pavlovsk Park]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Bridges in Pavlovsk Park]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Bridges over the Slavyanka River]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Bridges over the Slavyanka River]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Andrey Voronikhin]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Andrey Voronikhin]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;−&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #ffe49c; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Cultural heritage monuments in Pavlovsk, Saint Petersburg‎]]&lt;/div&gt;&lt;/td&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-empty&quot;&gt;&amp;#160;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Stone arch footbridges in Russia‎]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Stone arch footbridges in Russia‎]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Slavyanka River in Pavlovsk park]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Slavyanka River in Pavlovsk park]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;

&lt;!-- diff cache key commonswiki:diff:version:1.11a:oldid:235993042:newid:236000023 --&gt;
&lt;/table&gt;</summary>
		<author><name>Екатерина Борисова</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:Neuer_Wall_17.jpg&amp;diff=236000022&amp;oldid=0</id>
		<title>File:Neuer Wall 17.jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:Neuer_Wall_17.jpg&amp;diff=236000022&amp;oldid=0"/>
				<updated>2017-03-04T21:45:18Z</updated>
		
		<summary type="html">&lt;p&gt;&lt;a href=&quot;/wiki/User:Pauli-Pirat&quot; class=&quot;mw-userlink&quot; title=&quot;User:Pauli-Pirat&quot;&gt;&lt;bdi&gt;Pauli-Pirat&lt;/bdi&gt;&lt;/a&gt; uploaded &lt;a href=&quot;/wiki/File:Neuer_Wall_17.jpg&quot; title=&quot;File:Neuer Wall 17.jpg&quot;&gt;File:Neuer Wall 17.jpg&lt;/a&gt; User created page with UploadWizard&lt;/p&gt;
&lt;p&gt;&lt;b&gt;New page&lt;/b&gt;&lt;/p&gt;&lt;div&gt;=={{int:filedesc}}==&lt;br /&gt;
{{Information&lt;br /&gt;
|description={{de|1=Wohn und Geschäftshaus Neuer Wall 17, erbaut um 1845, danach mehrfach umgebaut.}}&lt;br /&gt;
|date=2017-03-03 14:36:27&lt;br /&gt;
|source={{own}}&lt;br /&gt;
|author=[[User:Pauli-Pirat|Pauli-Pirat]]&lt;br /&gt;
|permission=&lt;br /&gt;
|other versions=&lt;br /&gt;
}}&lt;br /&gt;
&lt;br /&gt;
=={{int:license-header}}==&lt;br /&gt;
{{self|cc-by-sa-4.0}}&lt;br /&gt;
&lt;br /&gt;
[[Category:Neuer Wall]]&lt;br /&gt;
[[Category:Images by Pauli-Pirat]]&lt;/div&gt;</summary>
		<author><name>Pauli-Pirat</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:Neuer_Wall_15_(2).jpg&amp;diff=236000020&amp;oldid=0</id>
		<title>File:Neuer Wall 15 (2).jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:Neuer_Wall_15_(2).jpg&amp;diff=236000020&amp;oldid=0"/>
				<updated>2017-03-04T21:45:18Z</updated>
		
		<summary type="html">&lt;p&gt;&lt;a href=&quot;/wiki/User:Pauli-Pirat&quot; class=&quot;mw-userlink&quot; title=&quot;User:Pauli-Pirat&quot;&gt;&lt;bdi&gt;Pauli-Pirat&lt;/bdi&gt;&lt;/a&gt; uploaded &lt;a href=&quot;/wiki/File:Neuer_Wall_15_(2).jpg&quot; title=&quot;File:Neuer Wall 15 (2).jpg&quot;&gt;File:Neuer Wall 15 (2).jpg&lt;/a&gt; User created page with UploadWizard&lt;/p&gt;
&lt;p&gt;&lt;b&gt;New page&lt;/b&gt;&lt;/p&gt;&lt;div&gt;=={{int:filedesc}}==&lt;br /&gt;
{{Information&lt;br /&gt;
|description={{de|1=Kontorhaus Neuer Wall 15, eingetragen als Ostindienhaus 1906-1907 ( Umbau ), Ausführung Ernst Friedheim.}}&lt;br /&gt;
|date=2017-03-03 14:36:49&lt;br /&gt;
|source={{own}}&lt;br /&gt;
|author=[[User:Pauli-Pirat|Pauli-Pirat]]&lt;br /&gt;
|permission=&lt;br /&gt;
|other versions=&lt;br /&gt;
}}&lt;br /&gt;
&lt;br /&gt;
=={{int:license-header}}==&lt;br /&gt;
{{self|cc-by-sa-4.0}}&lt;br /&gt;
&lt;br /&gt;
[[Category:Neuer Wall]]&lt;br /&gt;
[[Category:Images by Pauli-Pirat]]&lt;/div&gt;</summary>
		<author><name>Pauli-Pirat</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:US_Secret_Service_Vehicle_Inspection_Unit_(17192916986).jpg&amp;diff=236000021&amp;oldid=0</id>
		<title>File:US Secret Service Vehicle Inspection Unit (17192916986).jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:US_Secret_Service_Vehicle_Inspection_Unit_(17192916986).jpg&amp;diff=236000021&amp;oldid=0"/>
				<updated>2017-03-04T21:45:17Z</updated>
		
		<summary type="html">&lt;p&gt;&lt;a href=&quot;/wiki/User:XXN&quot; class=&quot;mw-userlink&quot; title=&quot;User:XXN&quot;&gt;&lt;bdi&gt;XXN&lt;/bdi&gt;&lt;/a&gt; uploaded &lt;a href=&quot;/wiki/File:US_Secret_Service_Vehicle_Inspection_Unit_(17192916986).jpg&quot; title=&quot;File:US Secret Service Vehicle Inspection Unit (17192916986).jpg&quot;&gt;File:US Secret Service Vehicle Inspection Unit (17192916986).jpg&lt;/a&gt; Transferred from Flickr via &lt;a href=&quot;/wiki/Commons:Flickr2Commons&quot; title=&quot;Commons:Flickr2Commons&quot;&gt;Flickr2Commons&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;New page&lt;/b&gt;&lt;/p&gt;&lt;div&gt;== {{int:filedesc}} ==&lt;br /&gt;
{{Information&lt;br /&gt;
| Description = A Secret Service Uniformed Division truck for vehicle inspections. Checkpoint was along road at the rear of the White House in Washington, DC.&lt;br /&gt;
| Source      = [https://www.flickr.com/photos/_schmal/17192916986/ US Secret Service Vehicle Inspection Unit]&lt;br /&gt;
| Date        = 2015-04-17 11:12&lt;br /&gt;
| Author      = [https://www.flickr.com/people/132192110@N04 Alex Smith] from Fort Collins, CO, United States&lt;br /&gt;
| Permission  = &lt;br /&gt;
| other_versions=&lt;br /&gt;
}}&lt;br /&gt;
&lt;br /&gt;
=={{int:license-header}}==&lt;br /&gt;
{{cc-zero}}&lt;br /&gt;
{{flickrreview}}&lt;br /&gt;
[[Category:Photographs by Alex Smith]]&lt;/div&gt;</summary>
		<author><name>XXN</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:Neuer_Wall_15.jpg&amp;diff=236000019&amp;oldid=0</id>
		<title>File:Neuer Wall 15.jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:Neuer_Wall_15.jpg&amp;diff=236000019&amp;oldid=0"/>
				<updated>2017-03-04T21:45:17Z</updated>
		
		<summary type="html">&lt;p&gt;&lt;a href=&quot;/wiki/User:Pauli-Pirat&quot; class=&quot;mw-userlink&quot; title=&quot;User:Pauli-Pirat&quot;&gt;&lt;bdi&gt;Pauli-Pirat&lt;/bdi&gt;&lt;/a&gt; uploaded &lt;a href=&quot;/wiki/File:Neuer_Wall_15.jpg&quot; title=&quot;File:Neuer Wall 15.jpg&quot;&gt;File:Neuer Wall 15.jpg&lt;/a&gt; User created page with UploadWizard&lt;/p&gt;
&lt;p&gt;&lt;b&gt;New page&lt;/b&gt;&lt;/p&gt;&lt;div&gt;=={{int:filedesc}}==&lt;br /&gt;
{{Information&lt;br /&gt;
|description={{de|1=Kontorhaus Neuer Wall 15, das Ostindienhaus. Eingetragen in der DL mit den Daten 1906-1907 ( Umbau ), Ausführung Ernst Friedheim.}}&lt;br /&gt;
|date=2017-03-03 14:37:10&lt;br /&gt;
|source={{own}}&lt;br /&gt;
|author=[[User:Pauli-Pirat|Pauli-Pirat]]&lt;br /&gt;
|permission=&lt;br /&gt;
|other versions=&lt;br /&gt;
}}&lt;br /&gt;
&lt;br /&gt;
=={{int:license-header}}==&lt;br /&gt;
{{self|cc-by-sa-4.0}}&lt;br /&gt;
&lt;br /&gt;
[[Category:Neuer Wall]]&lt;br /&gt;
[[Category:Images by Pauli-Pirat]]&lt;/div&gt;</summary>
		<author><name>Pauli-Pirat</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:LittleWoodstar.jpg&amp;diff=236000017&amp;oldid=0</id>
		<title>File:LittleWoodstar.jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:LittleWoodstar.jpg&amp;diff=236000017&amp;oldid=0"/>
				<updated>2017-03-04T21:45:16Z</updated>
		
		<summary type="html">&lt;p&gt;&lt;a href=&quot;/wiki/User:TonyCastro&quot; class=&quot;mw-userlink&quot; title=&quot;User:TonyCastro&quot;&gt;&lt;bdi&gt;TonyCastro&lt;/bdi&gt;&lt;/a&gt; uploaded &lt;a href=&quot;/wiki/File:LittleWoodstar.jpg&quot; title=&quot;File:LittleWoodstar.jpg&quot;&gt;File:LittleWoodstar.jpg&lt;/a&gt; User created page with UploadWizard&lt;/p&gt;
&lt;p&gt;&lt;b&gt;New page&lt;/b&gt;&lt;/p&gt;&lt;div&gt;=={{int:filedesc}}==&lt;br /&gt;
{{Information&lt;br /&gt;
|description={{en|1=female / Copalinga Lodge - Ecuador}}&lt;br /&gt;
|date=2013-02-20&lt;br /&gt;
|source={{own}}&lt;br /&gt;
|author=[[User:TonyCastro|TonyCastro]]&lt;br /&gt;
|permission=&lt;br /&gt;
|other versions=&lt;br /&gt;
}}&lt;br /&gt;
&lt;br /&gt;
http://seeworthybirds.com/Ecuador/4Hummingbirds/Little_Woodstar.htm&lt;br /&gt;
&lt;br /&gt;
=={{int:license-header}}==&lt;br /&gt;
{{self|cc-by-sa-4.0}}&lt;br /&gt;
&lt;br /&gt;
[[Category:Birds of Ecuador]]&lt;/div&gt;</summary>
		<author><name>TonyCastro</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:US_Secret_Service_Uniformed_Division_Chevy_Impala_(17192916716).jpg&amp;diff=236000016&amp;oldid=0</id>
		<title>File:US Secret Service Uniformed Division Chevy Impala (17192916716).jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:US_Secret_Service_Uniformed_Division_Chevy_Impala_(17192916716).jpg&amp;diff=236000016&amp;oldid=0"/>
				<updated>2017-03-04T21:45:15Z</updated>
		
		<summary type="html">&lt;p&gt;&lt;a href=&quot;/wiki/User:XXN&quot; class=&quot;mw-userlink&quot; title=&quot;User:XXN&quot;&gt;&lt;bdi&gt;XXN&lt;/bdi&gt;&lt;/a&gt; uploaded &lt;a href=&quot;/wiki/File:US_Secret_Service_Uniformed_Division_Chevy_Impala_(17192916716).jpg&quot; title=&quot;File:US Secret Service Uniformed Division Chevy Impala (17192916716).jpg&quot;&gt;File:US Secret Service Uniformed Division Chevy Impala (17192916716).jpg&lt;/a&gt; Transferred from Flickr via &lt;a href=&quot;/wiki/Commons:Flickr2Commons&quot; title=&quot;Commons:Flickr2Commons&quot;&gt;Flickr2Commons&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;New page&lt;/b&gt;&lt;/p&gt;&lt;div&gt;== {{int:filedesc}} ==&lt;br /&gt;
{{Information&lt;br /&gt;
| Description = US Secret Service Uniformed Division Chevy Impala&lt;br /&gt;
| Source      = [https://www.flickr.com/photos/_schmal/17192916716/ US Secret Service Uniformed Division Chevy Impala]&lt;br /&gt;
| Date        = 2015-04-17 11:11&lt;br /&gt;
| Author      = [https://www.flickr.com/people/132192110@N04 Alex Smith] from Fort Collins, CO, United States&lt;br /&gt;
| Permission  = &lt;br /&gt;
| other_versions=&lt;br /&gt;
}}&lt;br /&gt;
&lt;br /&gt;
=={{int:license-header}}==&lt;br /&gt;
{{cc-zero}}&lt;br /&gt;
{{flickrreview}}&lt;br /&gt;
[[Category:Photographs by Alex Smith]]&lt;/div&gt;</summary>
		<author><name>XXN</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:John_Henry_Haynes._Kari_Daiyu_(id.15168353).jpg&amp;diff=236000012&amp;oldid=0</id>
		<title>File:John Henry Haynes. Kari Daiyu (id.15168353).jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:John_Henry_Haynes._Kari_Daiyu_(id.15168353).jpg&amp;diff=236000012&amp;oldid=0"/>
				<updated>2017-03-04T21:45:14Z</updated>
		
		<summary type="html">&lt;p&gt;&lt;a href=&quot;/wiki/User:Geagea&quot; class=&quot;mw-userlink&quot; title=&quot;User:Geagea&quot;&gt;&lt;bdi&gt;Geagea&lt;/bdi&gt;&lt;/a&gt; uploaded a new version of &lt;a href=&quot;/wiki/File:John_Henry_Haynes._Kari_Daiyu_(id.15168353).jpg&quot; title=&quot;File:John Henry Haynes. Kari Daiyu (id.15168353).jpg&quot;&gt;File:John Henry Haynes. Kari Daiyu (id.15168353).jpg&lt;/a&gt; crop.+&lt;/p&gt;
&lt;p&gt;&lt;b&gt;New page&lt;/b&gt;&lt;/p&gt;&lt;div&gt;=={{int:filedesc}}==&lt;br /&gt;
{{Photograph&lt;br /&gt;
 |photographer       = {{Creator:John Henry Haynes}}&lt;br /&gt;
 |title              = &lt;br /&gt;
 |description        = {{en|1=Grand Portal of a Seljukian Khan or caravanserai at Kari Daiyu about 24 miles east of Caesarea in Cappadocia on the ancient road from Caesarea to Metilene, near the Euphrates. from: Robert G. Ousterhout: ''John Henry Haynes - A Photographer and Archaeologist in the Ottoman Empire 1881–1900'' S. 89 ISBN 978-605-62429-0-8}}&lt;br /&gt;
 |depicted people    = &lt;br /&gt;
 |depicted place     = &lt;br /&gt;
 |date               = 1887&lt;br /&gt;
 |medium             = albumen prints&lt;br /&gt;
 |dimensions         =&lt;br /&gt;
 |institution        = [[:en:Harvard Library|Harvard Library]], [http://library.harvard.edu/fal Fine Arts Library]&lt;br /&gt;
 |department         = Special Collections, Cambridge, Middlesex, Massachusetts, United States. HSM.HayC.109. AKP128.02&lt;br /&gt;
 |references         =&lt;br /&gt;
 |object history     =  &lt;br /&gt;
 |exhibition history =&lt;br /&gt;
 |credit line        =&lt;br /&gt;
 |inscriptions       =&lt;br /&gt;
 |notes              = Record Identifier: olvwork484383&lt;br /&gt;
 |accession number   =&lt;br /&gt;
 |source             = [http://via.lib.harvard.edu:80/via/deliver/deepLinkItem?recordId=olvwork484383&amp;amp;componentId=FHCL:3637157 via.lib.harvard.edu]&lt;br /&gt;
 |permission         =&lt;br /&gt;
 |other_versions     =&lt;br /&gt;
}}&lt;br /&gt;
&lt;br /&gt;
=={{int:license-header}}==&lt;br /&gt;
{{PD-scan|PD-old-auto-1923|deathyear=1910}}&lt;br /&gt;
&lt;br /&gt;
[[Category:John Henry Haynes]]&lt;/div&gt;</summary>
		<author><name>Geagea</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:NNational_Zoological_Park_Delhiational_Zoological_Park_376.JPG&amp;diff=236000014&amp;oldid=219838996</id>
		<title>File:NNational Zoological Park Delhiational Zoological Park 376.JPG</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:NNational_Zoological_Park_Delhiational_Zoological_Park_376.JPG&amp;diff=236000014&amp;oldid=219838996"/>
				<updated>2017-03-04T21:45:14Z</updated>
		
		<summary type="html">&lt;p&gt;added &lt;a href=&quot;/wiki/Category:Papio_hamadryas_in_zoos&quot; title=&quot;Category:Papio hamadryas in zoos&quot;&gt;Category:Papio hamadryas in zoos&lt;/a&gt; using &lt;a href=&quot;/wiki/Help:Gadget-HotCat&quot; title=&quot;Help:Gadget-HotCat&quot;&gt;HotCat&lt;/a&gt;&lt;/p&gt;
&lt;table class=&quot;diff diff-contentalign-left&quot; data-mw=&quot;interface&quot;&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;tr style='vertical-align: top;' lang='en'&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;← Older revision&lt;/td&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;Revision as of 21:45, 4 March 2017&lt;/td&gt;
				&lt;/tr&gt;&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 13:&lt;/td&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 13:&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Mammals in National Zoological Park Delhi]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Mammals in National Zoological Park Delhi]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-empty&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;+&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #a3d3ff; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Papio hamadryas in zoos]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;

&lt;!-- diff cache key commonswiki:diff:version:1.11a:oldid:219838996:newid:236000014 --&gt;
&lt;/table&gt;</summary>
		<author><name>Arnaud Palastowicz</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:Vix_Note_HU.JPG&amp;diff=236000013&amp;oldid=159299594</id>
		<title>File:Vix Note HU.JPG</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:Vix_Note_HU.JPG&amp;diff=236000013&amp;oldid=159299594"/>
				<updated>2017-03-04T21:45:14Z</updated>
		
		<summary type="html">&lt;p&gt;added &lt;a href=&quot;/wiki/Category:Belgrade_Truce&quot; title=&quot;Category:Belgrade Truce&quot;&gt;Category:Belgrade Truce&lt;/a&gt; using &lt;a href=&quot;/wiki/Help:Gadget-HotCat&quot; title=&quot;Help:Gadget-HotCat&quot;&gt;HotCat&lt;/a&gt;&lt;/p&gt;
&lt;table class=&quot;diff diff-contentalign-left&quot; data-mw=&quot;interface&quot;&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;tr style='vertical-align: top;' lang='en'&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;← Older revision&lt;/td&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;Revision as of 21:45, 4 March 2017&lt;/td&gt;
				&lt;/tr&gt;&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 12:&lt;/td&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 12:&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Hungarian Revolution of 1918-1919]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Hungarian Revolution of 1918-1919]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-empty&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;+&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #a3d3ff; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Belgrade Truce]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;== {{int:license-header}} ==&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;== {{int:license-header}} ==&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;{{self|GFDL|cc-by-sa-3.0}}&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;{{self|GFDL|cc-by-sa-3.0}}&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;

&lt;!-- diff cache key commonswiki:diff:version:1.11a:oldid:159299594:newid:236000013 --&gt;
&lt;/table&gt;</summary>
		<author><name>Accipiter Q. Gentilis</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:DC_Unmarked_Police_Tahoe_(17192917136).jpg&amp;diff=236000008&amp;oldid=0</id>
		<title>File:DC Unmarked Police Tahoe (17192917136).jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:DC_Unmarked_Police_Tahoe_(17192917136).jpg&amp;diff=236000008&amp;oldid=0"/>
				<updated>2017-03-04T21:45:10Z</updated>
		
		<summary type="html">&lt;p&gt;&lt;a href=&quot;/wiki/User:XXN&quot; class=&quot;mw-userlink&quot; title=&quot;User:XXN&quot;&gt;&lt;bdi&gt;XXN&lt;/bdi&gt;&lt;/a&gt; uploaded &lt;a href=&quot;/wiki/File:DC_Unmarked_Police_Tahoe_(17192917136).jpg&quot; title=&quot;File:DC Unmarked Police Tahoe (17192917136).jpg&quot;&gt;File:DC Unmarked Police Tahoe (17192917136).jpg&lt;/a&gt; Transferred from Flickr via &lt;a href=&quot;/wiki/Commons:Flickr2Commons&quot; title=&quot;Commons:Flickr2Commons&quot;&gt;Flickr2Commons&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;New page&lt;/b&gt;&lt;/p&gt;&lt;div&gt;== {{int:filedesc}} ==&lt;br /&gt;
{{Information&lt;br /&gt;
| Description = DC Unmarked Police Tahoe&lt;br /&gt;
| Source      = [https://www.flickr.com/photos/_schmal/17192917136/ DC Unmarked Police Tahoe]&lt;br /&gt;
| Date        = 2015-04-17 11:23&lt;br /&gt;
| Author      = [https://www.flickr.com/people/132192110@N04 Alex Smith] from Fort Collins, CO, United States&lt;br /&gt;
| Permission  = &lt;br /&gt;
| other_versions=&lt;br /&gt;
}}&lt;br /&gt;
&lt;br /&gt;
=={{int:license-header}}==&lt;br /&gt;
{{cc-zero}}&lt;br /&gt;
{{flickrreview}}&lt;br /&gt;
[[Category:Photographs by Alex Smith]]&lt;/div&gt;</summary>
		<author><name>XXN</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:Washington,_DC_Protective_Services_Chevy_Impala_(17218303591).jpg&amp;diff=236000009&amp;oldid=0</id>
		<title>File:Washington, DC Protective Services Chevy Impala (17218303591).jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:Washington,_DC_Protective_Services_Chevy_Impala_(17218303591).jpg&amp;diff=236000009&amp;oldid=0"/>
				<updated>2017-03-04T21:45:10Z</updated>
		
		<summary type="html">&lt;p&gt;&lt;a href=&quot;/wiki/User:XXN&quot; class=&quot;mw-userlink&quot; title=&quot;User:XXN&quot;&gt;&lt;bdi&gt;XXN&lt;/bdi&gt;&lt;/a&gt; uploaded &lt;a href=&quot;/wiki/File:Washington,_DC_Protective_Services_Chevy_Impala_(17218303591).jpg&quot; title=&quot;File:Washington, DC Protective Services Chevy Impala (17218303591).jpg&quot;&gt;File:Washington, DC Protective Services Chevy Impala (17218303591).jpg&lt;/a&gt; Transferred from Flickr via &lt;a href=&quot;/wiki/Commons:Flickr2Commons&quot; title=&quot;Commons:Flickr2Commons&quot;&gt;Flickr2Commons&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;New page&lt;/b&gt;&lt;/p&gt;&lt;div&gt;== {{int:filedesc}} ==&lt;br /&gt;
{{Information&lt;br /&gt;
| Description = Washington, DC Protective Services Chevy Impala&lt;br /&gt;
| Source      = [https://www.flickr.com/photos/_schmal/17218303591/ Washington, DC Protective Services Chevy Impala]&lt;br /&gt;
| Date        = 2015-04-17 14:38&lt;br /&gt;
| Author      = [https://www.flickr.com/people/132192110@N04 Alex Smith] from Fort Collins, CO, United States&lt;br /&gt;
| Permission  = &lt;br /&gt;
| other_versions=&lt;br /&gt;
}}&lt;br /&gt;
&lt;br /&gt;
=={{int:license-header}}==&lt;br /&gt;
{{cc-zero}}&lt;br /&gt;
{{flickrreview}}&lt;br /&gt;
[[Category:Photographs by Alex Smith]]&lt;/div&gt;</summary>
		<author><name>XXN</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=Category:Kyn%C5%A1perk_nad_Oh%C5%99%C3%AD_(train_station)&amp;diff=236000007&amp;oldid=184247026</id>
		<title>Category:Kynšperk nad Ohří (train station)</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=Category:Kyn%C5%A1perk_nad_Oh%C5%99%C3%AD_(train_station)&amp;diff=236000007&amp;oldid=184247026"/>
				<updated>2017-03-04T21:45:09Z</updated>
		
		<summary type="html">&lt;p&gt;added &lt;a href=&quot;/wiki/Category:Kyn%C5%A1perk_nad_Oh%C5%99%C3%AD&quot; title=&quot;Category:Kynšperk nad Ohří&quot;&gt;Category:Kynšperk nad Ohří&lt;/a&gt; using &lt;a href=&quot;/wiki/Help:Gadget-HotCat&quot; title=&quot;Help:Gadget-HotCat&quot;&gt;HotCat&lt;/a&gt;&lt;/p&gt;
&lt;table class=&quot;diff diff-contentalign-left&quot; data-mw=&quot;interface&quot;&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;tr style='vertical-align: top;' lang='en'&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;← Older revision&lt;/td&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;Revision as of 21:45, 4 March 2017&lt;/td&gt;
				&lt;/tr&gt;&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 1:&lt;/td&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 1:&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Railway line 140 (Czech Republic)]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Railway line 140 (Czech Republic)]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-empty&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;+&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #a3d3ff; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Kynšperk nad Ohří]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;

&lt;!-- diff cache key commonswiki:diff:version:1.11a:oldid:184247026:newid:236000007 --&gt;
&lt;/table&gt;</summary>
		<author><name>Palickap</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:Heidenau,_Germany_-_panoramio_(3).jpg&amp;diff=236000006&amp;oldid=0</id>
		<title>File:Heidenau, Germany - panoramio (3).jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:Heidenau,_Germany_-_panoramio_(3).jpg&amp;diff=236000006&amp;oldid=0"/>
				<updated>2017-03-04T21:45:08Z</updated>
		
		<summary type="html">&lt;p&gt;&lt;a href=&quot;/wiki/User:Panoramio_upload_bot&quot; class=&quot;mw-userlink&quot; title=&quot;User:Panoramio upload bot&quot;&gt;&lt;bdi&gt;Panoramio upload bot&lt;/bdi&gt;&lt;/a&gt; uploaded &lt;a href=&quot;/wiki/File:Heidenau,_Germany_-_panoramio_(3).jpg&quot; title=&quot;File:Heidenau, Germany - panoramio (3).jpg&quot;&gt;File:Heidenau, Germany - panoramio (3).jpg&lt;/a&gt; == {{int:filedesc}} == {{Information |description=Heidenau, Germany |date={{Taken on|2013-08-07}} |source=http://www.panoramio.com/photo/94315879 |author=[http://www.panoramio.com/user/7778207?with_photo_id=94315879 Kalispera Dell] |permission={{cc-by-...&lt;/p&gt;
&lt;p&gt;&lt;b&gt;New page&lt;/b&gt;&lt;/p&gt;&lt;div&gt;== {{int:filedesc}} ==&lt;br /&gt;
{{Information&lt;br /&gt;
|description=Heidenau, Germany&lt;br /&gt;
|date={{Taken on|2013-08-07}}&lt;br /&gt;
|source=http://www.panoramio.com/photo/94315879&lt;br /&gt;
|author=[http://www.panoramio.com/user/7778207?with_photo_id=94315879 Kalispera Dell]&lt;br /&gt;
|permission={{cc-by-3.0|Kalispera Dell}}&lt;br /&gt;
{{Panoramioreview|Panoramio_upload_bot|2017-03-04}}&lt;br /&gt;
|other_versions=&lt;br /&gt;
|other_fields={{Information field|Name=Tags&amp;lt;br /&amp;gt;(from Panoramio photo page)|Value=&amp;lt;code&amp;gt;Heidenau&amp;lt;/code&amp;gt;, &amp;lt;code&amp;gt;Heidenau&amp;lt;/code&amp;gt;}}&lt;br /&gt;
}}&lt;br /&gt;
{{Location|50.980039|13.853001|source:Panoramio}}&lt;br /&gt;
&lt;br /&gt;
{{Check categories|year=2017|month=March|day=4}}&lt;br /&gt;
&lt;br /&gt;
[[Category:Heidenau]]&lt;br /&gt;
[[Category:Heidenau]]&lt;br /&gt;
[[Category:Panoramio files uploaded by Panoramio_upload_bot]]&lt;/div&gt;</summary>
		<author><name>Panoramio upload bot</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:Unioniste_Lefaucheux.jpg&amp;diff=236000004&amp;oldid=215993019</id>
		<title>File:Unioniste Lefaucheux.jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:Unioniste_Lefaucheux.jpg&amp;diff=236000004&amp;oldid=215993019"/>
				<updated>2017-03-04T21:45:08Z</updated>
		
		<summary type="html">&lt;p&gt;added &lt;a href=&quot;/wiki/Category:People_with_revolvers&quot; title=&quot;Category:People with revolvers&quot;&gt;Category:People with revolvers&lt;/a&gt; using &lt;a href=&quot;/wiki/Help:Gadget-HotCat&quot; title=&quot;Help:Gadget-HotCat&quot;&gt;HotCat&lt;/a&gt;&lt;/p&gt;
&lt;table class=&quot;diff diff-contentalign-left&quot; data-mw=&quot;interface&quot;&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;tr style='vertical-align: top;' lang='en'&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;← Older revision&lt;/td&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;Revision as of 21:45, 4 March 2017&lt;/td&gt;
				&lt;/tr&gt;&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 15:&lt;/td&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 15:&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:American Civil War cavalry]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:American Civil War cavalry]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Lefaucheux revolver]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Lefaucheux revolver]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-empty&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;+&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #a3d3ff; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:People with revolvers]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;

&lt;!-- diff cache key commonswiki:diff:version:1.11a:oldid:215993019:newid:236000004 --&gt;
&lt;/table&gt;</summary>
		<author><name>SunOfErat</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:Thomas_Benjamin_Kennington_-_Orphans.jpg&amp;diff=236000003&amp;oldid=234311743</id>
		<title>File:Thomas Benjamin Kennington - Orphans.jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:Thomas_Benjamin_Kennington_-_Orphans.jpg&amp;diff=236000003&amp;oldid=234311743"/>
				<updated>2017-03-04T21:45:07Z</updated>
		
		<summary type="html">&lt;p&gt;removed &lt;a href=&quot;/wiki/Category:19th-century_genre_paintings&quot; title=&quot;Category:19th-century genre paintings&quot;&gt;Category:19th-century genre paintings&lt;/a&gt;; added &lt;a href=&quot;/wiki/Category:1880s_genre_paintings&quot; title=&quot;Category:1880s genre paintings&quot;&gt;Category:1880s genre paintings&lt;/a&gt; using &lt;a href=&quot;/wiki/Help:Gadget-HotCat&quot; title=&quot;Help:Gadget-HotCat&quot;&gt;HotCat&lt;/a&gt;&lt;/p&gt;
&lt;table class=&quot;diff diff-contentalign-left&quot; data-mw=&quot;interface&quot;&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;tr style='vertical-align: top;' lang='en'&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;← Older revision&lt;/td&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;Revision as of 21:45, 4 March 2017&lt;/td&gt;
				&lt;/tr&gt;&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 35:&lt;/td&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 35:&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Paintings in Tate Britain]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Paintings in Tate Britain]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:19th-century childhood in painting]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:19th-century childhood in painting]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;−&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #ffe49c; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:&lt;del class=&quot;diffchange diffchange-inline&quot;&gt;19th-century&lt;/del&gt; genre paintings]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;+&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #a3d3ff; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:&lt;ins class=&quot;diffchange diffchange-inline&quot;&gt;1880s&lt;/ins&gt; genre paintings]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;

&lt;!-- diff cache key commonswiki:diff:version:1.11a:oldid:234311743:newid:236000003 --&gt;
&lt;/table&gt;</summary>
		<author><name>Hilohello</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:Bellingham,_WA_Police_BMW_Motorcycle_(17228478821).jpg&amp;diff=236000002&amp;oldid=0</id>
		<title>File:Bellingham, WA Police BMW Motorcycle (17228478821).jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:Bellingham,_WA_Police_BMW_Motorcycle_(17228478821).jpg&amp;diff=236000002&amp;oldid=0"/>
				<updated>2017-03-04T21:45:06Z</updated>
		
		<summary type="html">&lt;p&gt;&lt;a href=&quot;/wiki/User:XXN&quot; class=&quot;mw-userlink&quot; title=&quot;User:XXN&quot;&gt;&lt;bdi&gt;XXN&lt;/bdi&gt;&lt;/a&gt; uploaded &lt;a href=&quot;/wiki/File:Bellingham,_WA_Police_BMW_Motorcycle_(17228478821).jpg&quot; title=&quot;File:Bellingham, WA Police BMW Motorcycle (17228478821).jpg&quot;&gt;File:Bellingham, WA Police BMW Motorcycle (17228478821).jpg&lt;/a&gt; Transferred from Flickr via &lt;a href=&quot;/wiki/Commons:Flickr2Commons&quot; title=&quot;Commons:Flickr2Commons&quot;&gt;Flickr2Commons&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;New page&lt;/b&gt;&lt;/p&gt;&lt;div&gt;== {{int:filedesc}} ==&lt;br /&gt;
{{Information&lt;br /&gt;
| Description = A Bellingham, WA Police Motor Unit officer conducts a traffic stop on Magnolia street.&lt;br /&gt;
| Source      = [https://www.flickr.com/photos/_schmal/17228478821/ Bellingham, WA Police BMW Motorcycle]&lt;br /&gt;
| Date        = 2015-04-21 12:18&lt;br /&gt;
| Author      = [https://www.flickr.com/people/132192110@N04 Alex Smith] from Fort Collins, CO, United States&lt;br /&gt;
| Permission  = &lt;br /&gt;
| other_versions=&lt;br /&gt;
}}&lt;br /&gt;
&lt;br /&gt;
=={{int:license-header}}==&lt;br /&gt;
{{cc-zero}}&lt;br /&gt;
{{flickrreview}}&lt;br /&gt;
[[Category:Photographs by Alex Smith]]&lt;/div&gt;</summary>
		<author><name>XXN</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=Commons:Featured_picture_candidates/File:Edita_Malov%C4%8Di%C4%87_%C3%96sterreichischer_Filmpreis_2017_2.jpg&amp;diff=236000001&amp;oldid=235999957</id>
		<title>Commons:Featured picture candidates/File:Edita Malovčić Österreichischer Filmpreis 2017 2.jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=Commons:Featured_picture_candidates/File:Edita_Malov%C4%8Di%C4%87_%C3%96sterreichischer_Filmpreis_2017_2.jpg&amp;diff=236000001&amp;oldid=235999957"/>
				<updated>2017-03-04T21:45:06Z</updated>
		
		<summary type="html">&lt;p&gt;‎&lt;span dir=&quot;auto&quot;&gt;&lt;span class=&quot;autocomment&quot;&gt;File:Edita Malovčić Österreichischer Filmpreis 2017 2.jpg&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;
&lt;table class=&quot;diff diff-contentalign-left&quot; data-mw=&quot;interface&quot;&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;tr style='vertical-align: top;' lang='en'&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;← Older revision&lt;/td&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;Revision as of 21:45, 4 March 2017&lt;/td&gt;
				&lt;/tr&gt;&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 13:&lt;/td&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 13:&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;*{{Info}} created and uploaded by [[User:Tsui|Tsui]] - nominated by [[User:Christian Ferrer|Christian Ferrer]] -- &amp;lt;span style=&quot;font-family: Constantia;&quot;&amp;gt;''[[User:Christian Ferrer|Christian Ferrer]] &amp;lt;sup&amp;gt;([[User talk:Christian Ferrer|talk]])&amp;lt;/sup&amp;gt;''&amp;lt;/span&amp;gt; 21:01, 4 March 2017 (UTC)&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;*{{Info}} created and uploaded by [[User:Tsui|Tsui]] - nominated by [[User:Christian Ferrer|Christian Ferrer]] -- &amp;lt;span style=&quot;font-family: Constantia;&quot;&amp;gt;''[[User:Christian Ferrer|Christian Ferrer]] &amp;lt;sup&amp;gt;([[User talk:Christian Ferrer|talk]])&amp;lt;/sup&amp;gt;''&amp;lt;/span&amp;gt; 21:01, 4 March 2017 (UTC)&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;*{{Support}} -- &amp;lt;span style=&quot;font-family: Constantia;&quot;&amp;gt;''[[User:Christian Ferrer|Christian Ferrer]] &amp;lt;sup&amp;gt;([[User talk:Christian Ferrer|talk]])&amp;lt;/sup&amp;gt;''&amp;lt;/span&amp;gt; 21:01, 4 March 2017 (UTC)&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;*{{Support}} -- &amp;lt;span style=&quot;font-family: Constantia;&quot;&amp;gt;''[[User:Christian Ferrer|Christian Ferrer]] &amp;lt;sup&amp;gt;([[User talk:Christian Ferrer|talk]])&amp;lt;/sup&amp;gt;''&amp;lt;/span&amp;gt; 21:01, 4 March 2017 (UTC)&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-empty&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;+&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #a3d3ff; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;*{{s}} Nice portrait. [[User:Yann|Yann]] ([[User talk:Yann|&amp;lt;span class=&quot;signature-talk&quot;&amp;gt;{{int:Talkpagelinktext}}&amp;lt;/span&amp;gt;]]) 21:45, 4 March 2017 (UTC)&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;

&lt;!-- diff cache key commonswiki:diff:version:1.11a:oldid:235999957:newid:236000001 --&gt;
&lt;/table&gt;</summary>
		<author><name>Yann</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=Category:Eastern_Catholic_saints&amp;diff=235999999&amp;oldid=120761918</id>
		<title>Category:Eastern Catholic saints</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=Category:Eastern_Catholic_saints&amp;diff=235999999&amp;oldid=120761918"/>
				<updated>2017-03-04T21:45:06Z</updated>
		
		<summary type="html">&lt;p&gt;&lt;/p&gt;
&lt;table class=&quot;diff diff-contentalign-left&quot; data-mw=&quot;interface&quot;&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;tr style='vertical-align: top;' lang='en'&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;← Older revision&lt;/td&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;Revision as of 21:45, 4 March 2017&lt;/td&gt;
				&lt;/tr&gt;&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 1:&lt;/td&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 1:&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;−&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #ffe49c; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;&lt;del class=&quot;diffchange diffchange-inline&quot;&gt;[[&lt;/del&gt;Category&lt;del class=&quot;diffchange diffchange-inline&quot;&gt;:Eastern&lt;/del&gt; &lt;del class=&quot;diffchange diffchange-inline&quot;&gt;Catholics&lt;/del&gt;|&lt;del class=&quot;diffchange diffchange-inline&quot;&gt;Saints]]&lt;/del&gt;&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;+&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #a3d3ff; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;&lt;ins class=&quot;diffchange diffchange-inline&quot;&gt;{{&lt;/ins&gt;Category &lt;ins class=&quot;diffchange diffchange-inline&quot;&gt;redirect&lt;/ins&gt;|&lt;ins class=&quot;diffchange diffchange-inline&quot;&gt;Roman Catholic saints}}&lt;/ins&gt;&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;−&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #ffe49c; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Roman Catholic saints]]&lt;/div&gt;&lt;/td&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-empty&quot;&gt;&amp;#160;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;−&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #ffe49c; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Christian saints by denomination]]&lt;/div&gt;&lt;/td&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-empty&quot;&gt;&amp;#160;&lt;/td&gt;
&lt;/tr&gt;

&lt;!-- diff cache key commonswiki:diff:version:1.11a:oldid:120761918:newid:235999999 --&gt;
&lt;/table&gt;</summary>
		<author><name>Cathy Richards</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:Bras%C3%A3o_de_Sert%C3%A3ozinho.jpg&amp;diff=235999998&amp;oldid=235773593</id>
		<title>File:Brasão de Sertãozinho.jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:Bras%C3%A3o_de_Sert%C3%A3ozinho.jpg&amp;diff=235999998&amp;oldid=235773593"/>
				<updated>2017-03-04T21:45:05Z</updated>
		
		<summary type="html">&lt;p&gt;&lt;/p&gt;
&lt;table class=&quot;diff diff-contentalign-left&quot; data-mw=&quot;interface&quot;&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;tr style='vertical-align: top;' lang='en'&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;← Older revision&lt;/td&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;Revision as of 21:45, 4 March 2017&lt;/td&gt;
				&lt;/tr&gt;&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 4:&lt;/td&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 4:&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;|date=2015-01-06 21:58:31&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;|date=2015-01-06 21:58:31&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;|source={{own}}&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;|source={{own}}&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;−&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #ffe49c; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;|author=&lt;del class=&quot;diffchange diffchange-inline&quot;&gt;[[User:Michael Johnes|Michael Johnes]]&lt;/del&gt;&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;+&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #a3d3ff; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;|author=&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;−&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #ffe49c; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;|permission=&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;+&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #a3d3ff; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;|permission=&lt;ins class=&quot;diffchange diffchange-inline&quot;&gt;{{PD-BrazilGov}}{{Insignia}}&lt;/ins&gt;&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;|other versions=&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;|other versions=&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;}}&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;}}&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;−&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #ffe49c; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;/td&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-empty&quot;&gt;&amp;#160;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;−&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #ffe49c; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;=={{int:license-header}}==&lt;/div&gt;&lt;/td&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-empty&quot;&gt;&amp;#160;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;−&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #ffe49c; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;{{self|cc-by-sa-4.0}}&lt;/div&gt;&lt;/td&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-empty&quot;&gt;&amp;#160;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;/td&gt;
&lt;/tr&gt;

&lt;!-- diff cache key commonswiki:diff:version:1.11a:oldid:235773593:newid:235999998 --&gt;
&lt;/table&gt;</summary>
		<author><name>Geovani.s</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:Hazlehurst_city_limit_US221NB.JPG&amp;diff=235999997&amp;oldid=224262493</id>
		<title>File:Hazlehurst city limit US221NB.JPG</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:Hazlehurst_city_limit_US221NB.JPG&amp;diff=235999997&amp;oldid=224262493"/>
				<updated>2017-03-04T21:45:05Z</updated>
		
		<summary type="html">&lt;p&gt;-&lt;a href=&quot;/wiki/Category:Transport_in_Jeff_Davis_County,_Georgia&quot; title=&quot;Category:Transport in Jeff Davis County, Georgia&quot;&gt;Category:Transport in Jeff Davis County, Georgia&lt;/a&gt;; ±&lt;a href=&quot;/wiki/Category:Georgia_State_Route_135&quot; title=&quot;Category:Georgia State Route 135&quot;&gt;Category:Georgia State Route 135&lt;/a&gt;→&lt;a href=&quot;/wiki/Category:Georgia_State_Route_135_in_Jeff_Davis_County,_Georgia&quot; title=&quot;Category:Georgia State Route 135 in Jeff Davis County, Georgia&quot;&gt;Category:Georgia State Route 135 in Jeff Davis County, Georgia&lt;/a&gt; using &lt;a href=&quot;/wiki/Help:Gadget-HotCat&quot; title=&quot;Help:Gadget-HotCat&quot;&gt;HotCat&lt;/a&gt;&lt;/p&gt;
&lt;table class=&quot;diff diff-contentalign-left&quot; data-mw=&quot;interface&quot;&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;col class='diff-marker' /&gt;
				&lt;col class='diff-content' /&gt;
				&lt;tr style='vertical-align: top;' lang='en'&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;← Older revision&lt;/td&gt;
				&lt;td colspan='2' style=&quot;background-color: white; color:black; text-align: center;&quot;&gt;Revision as of 21:45, 4 March 2017&lt;/td&gt;
				&lt;/tr&gt;&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 14:&lt;/td&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 14:&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:December 2015 in Georgia (U.S. state)]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:December 2015 in Georgia (U.S. state)]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-empty&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;+&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #a3d3ff; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Georgia State Route 135 in Jeff Davis County, Georgia]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-empty&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;+&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #a3d3ff; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Green and white rectangular signs]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Photographs taken on 2015-12-12|Hazlehurst, Georgia]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Photographs taken on 2015-12-12|Hazlehurst, Georgia]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Pictures by Mjrmtg]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Pictures by Mjrmtg]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 19:&lt;/td&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-lineno&quot;&gt;Line 21:&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Taken with Sony DSC-W830]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Taken with Sony DSC-W830]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:City signs in Georgia (U.S. state)]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:City signs in Georgia (U.S. state)]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;−&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #ffe49c; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Georgia State Route 135]]&lt;/div&gt;&lt;/td&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-empty&quot;&gt;&amp;#160;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;−&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #ffe49c; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Green and white rectangular signs]]&lt;/div&gt;&lt;/td&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-empty&quot;&gt;&amp;#160;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;−&lt;/td&gt;
  &lt;td style=&quot;color:black; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #ffe49c; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Transport in Jeff Davis County, Georgia]]&lt;/div&gt;&lt;/td&gt;
  &lt;td colspan=&quot;2&quot; class=&quot;diff-empty&quot;&gt;&amp;#160;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:U.S. Route 221 in Georgia]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:U.S. Route 221 in Georgia]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Hazlehurst, Georgia]]&lt;/div&gt;&lt;/td&gt;
  &lt;td class=&quot;diff-marker&quot;&gt;&amp;#160;&lt;/td&gt;
  &lt;td style=&quot;background-color: #f9f9f9; color: #333333; font-size: 88%; border-style: solid; border-width: 1px 1px 1px 4px; border-radius: 0.33em; border-color: #e6e6e6; vertical-align: top; white-space: pre-wrap;&quot;&gt;&lt;div&gt;[[Category:Hazlehurst, Georgia]]&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;

&lt;!-- diff cache key commonswiki:diff:version:1.11a:oldid:224262493:newid:235999997 --&gt;
&lt;/table&gt;</summary>
		<author><name>Mjrmtg</name></author>	</entry>

	<entry>
		<id>https://commons.wikimedia.org/w/index.php?title=File:Washington,_DC_Metro_Police_Ford_Police_Interceptor_(17192919416).jpg&amp;diff=235999994&amp;oldid=0</id>
		<title>File:Washington, DC Metro Police Ford Police Interceptor (17192919416).jpg</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/w/index.php?title=File:Washington,_DC_Metro_Police_Ford_Police_Interceptor_(17192919416).jpg&amp;diff=235999994&amp;oldid=0"/>
				<updated>2017-03-04T21:45:03Z</updated>
		
		<summary type="html">&lt;p&gt;&lt;a href=&quot;/wiki/User:XXN&quot; class=&quot;mw-userlink&quot; title=&quot;User:XXN&quot;&gt;&lt;bdi&gt;XXN&lt;/bdi&gt;&lt;/a&gt; uploaded &lt;a href=&quot;/wiki/File:Washington,_DC_Metro_Police_Ford_Police_Interceptor_(17192919416).jpg&quot; title=&quot;File:Washington, DC Metro Police Ford Police Interceptor (17192919416).jpg&quot;&gt;File:Washington, DC Metro Police Ford Police Interceptor (17192919416).jpg&lt;/a&gt; Transferred from Flickr via &lt;a href=&quot;/wiki/Commons:Flickr2Commons&quot; title=&quot;Commons:Flickr2Commons&quot;&gt;Flickr2Commons&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;New page&lt;/b&gt;&lt;/p&gt;&lt;div&gt;== {{int:filedesc}} ==&lt;br /&gt;
{{Information&lt;br /&gt;
| Description = Washington, DC Metro Police Ford Police Interceptor&lt;br /&gt;
| Source      = [https://www.flickr.com/photos/_schmal/17192919416/ Washington, DC Metro Police Ford Police Interceptor]&lt;br /&gt;
| Date        = 2015-04-17 14:57&lt;br /&gt;
| Author      = [https://www.flickr.com/people/132192110@N04 Alex Smith] from Fort Collins, CO, United States&lt;br /&gt;
| Permission  = &lt;br /&gt;
| other_versions=&lt;br /&gt;
}}&lt;br /&gt;
&lt;br /&gt;
=={{int:license-header}}==&lt;br /&gt;
{{cc-zero}}&lt;br /&gt;
{{flickrreview}}&lt;br /&gt;
[[Category:Photographs by Alex Smith]]&lt;/div&gt;</summary>
		<author><name>XXN</name></author>	</entry>

	</feed>