1
00:00:00,000 --> 00:00:05,000
Todos los australianos alegrémonos,

2
00:00:05,000 --> 00:00:10,000
Porque somos jóvenes y libres;

3
00:00:10,000 --> 00:00:15,000
Tenemos un suelo dorado y la riqueza para el progreso,

4
00:00:15,000 --> 00:00:20,000
Nuestro hogar está ceñido por el mar;

5
00:00:20,000 --> 00:00:25,000
Nuestra tierra abunda en los dones de la Naturaleza

6
00:00:25,000 --> 00:00:30,000
De una belleza rica y excepcional;

7
00:00:30,000 --> 00:00:35,000
En la página de la historia, en cada etapa

8
00:00:35,000 --> 00:00:40,000
¡Avanza, Australia justa!

9
00:00:40,000 --> 00:00:45,000
En las cepas alegres déjennos cantar:

10
00:00:45,000 --> 00:00:52,000
¡Avanza, Australia justa!