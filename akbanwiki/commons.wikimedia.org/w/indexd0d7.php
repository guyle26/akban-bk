1
00:00:00,000 --> 00:00:05,000
Australians all let us rejoice,

2
00:00:05,000 --> 00:00:10,000
For we are young and free;

3
00:00:10,000 --> 00:00:15,000
We've golden soil and wealth for toil;

4
00:00:15,000 --> 00:00:20,000
Our home is girt by sea;

5
00:00:20,000 --> 00:00:25,000
Our land abounds in nature's gifts

6
00:00:25,000 --> 00:00:30,000
Of beauty rich and rare;

7
00:00:30,000 --> 00:00:35,000
In history's page, let every stage

8
00:00:35,000 --> 00:00:40,000
Advance Australia Fair.

9
00:00:40,000 --> 00:00:45,000
In joyful strains then let us sing,

10
00:00:45,000 --> 00:00:52,000
Advance Australia Fair.