1
00:00:00,000 --> 00:00:02,500
Anchors Aweigh,

2
00:00:02,500 --> 00:00:04,500
my boys!

3
00:00:04,500 --> 00:00:09,000
Anchors Aweigh!

4
00:00:09,000 --> 00:00:12,500
Farewell to college joys,

5
00:00:12,500 --> 00:00:16,000
we sail at break of day

6
00:00:16,000 --> 00:00:18,000
-ay-ay-ay.

7
00:00:18,000 --> 00:00:23,000
Through our last night on shore,

8
00:00:23,000 --> 00:00:27,000
drink to the foam!

9
00:00:27,000 --> 00:00:31,000
Until we meet once more,

10
00:00:31,000 --> 00:00:36,000
here's wishing you a happy voyage home.

11
00:00:50,000 --> 00:00:54,500
Stand, Navy! Out to sea,

12
00:00:54,500 --> 00:00:58,000
fight our battle cry!

13
00:00:58,000 --> 00:01:08,000
We'll never change our course, So vicious foe steer shy-y-y-y.

14
00:01:08,000 --> 00:01:22,400
Roll out the TNT, Anchors Aweigh. Sail on to victory

15
00:01:22,000 --> 00:01:25,400
And sink their bones to Davy Jones, hooray!