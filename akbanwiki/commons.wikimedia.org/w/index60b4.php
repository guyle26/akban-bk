1
00:00:00,000 --> 00:00:05,000
Này dân Úc chúng ta hãy cùng nhau mừng vui

2
00:00:05,000 --> 00:00:10,000
Vì tuổi trẻ và tự do của chúng ta;

3
00:00:10,000 --> 00:00:15,000
Chúng ta có nền đất vàng và sự thịnh vượng nhờ lao động vất vả

4
00:00:15,000 --> 00:00:20,000
Nhà chúng ta được biển bao quanh;

5
00:00:20,000 --> 00:00:25,000
Đất chúng ta tràn trề quà của Thiên nhiên

6
00:00:25,000 --> 00:00:30,000
Một vẻ đẹp trù phú và quý hiếm;

7
00:00:30,000 --> 00:00:35,000
Trên trang sách lịch sử, hãy cho mỗi bước

8
00:00:35,000 --> 00:00:40,000
Làm tiến lên người đẹp Australia.

9
00:00:40,000 --> 00:00:45,000
Với một giọng vui mừng chúng ta hãy hát

10
00:00:45,000 --> 00:00:52,000
"Hãy tiến lên người đẹp Australia!"