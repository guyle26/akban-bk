1
00:00:00,000 --> 00:00:05,000
Australijczycy, cieszmy się,

2
00:00:05,000 --> 00:00:10,000
że jesteśmy młodzi i wolni.

3
00:00:10,000 --> 00:00:15,000
Mamy cenną ziemię i bogactwo za pracę.

4
00:00:15,000 --> 00:00:20,000
Nasz dom jest opasany przez morze,

5
00:00:20,000 --> 00:00:25,000
Nasz kraj przepełniają dary natury

6
00:00:25,000 --> 00:00:30,000
niezwykłej piękności i obfitości.

7
00:00:30,000 --> 00:00:35,000
Na karcie historii, każdym jej etapie,

8
00:00:35,000 --> 00:00:40,000
Naprzód, Piękna Australio.

9
00:00:40,000 --> 00:00:45,000
Zaśpiewajmy zatem wesoło te słowa:

10
00:00:45,000 --> 00:00:52,000
Naprzód, Piękna Australio.