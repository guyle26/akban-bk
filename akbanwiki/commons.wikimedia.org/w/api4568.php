<?xml version="1.0"?>
<feed xmlns="http://www.w3.org/2005/Atom" xml:lang="en">
	<id>https://commons.wikimedia.org/w/api.php?action=featuredfeed&amp;feed=potd&amp;feedformat=atom</id>
	<title>Wikimedia Commons picture of the day feed</title>
	<link rel="self" type="application/atom+xml" href="https://commons.wikimedia.org/w/api.php?action=featuredfeed&amp;feed=potd&amp;feedformat=atom"/>
	<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/wiki/Main_Page"/>
	<updated>2018-04-10T08:43:18Z</updated>
	<subtitle>Some of the finest images on Wikimedia Commons</subtitle>
	<generator>MediaWiki 1.31.0-wmf.28</generator>
	<entry>
		<id>https://commons.wikimedia.org/wiki/Special:FeedItem/potd/20180401000000/en</id>
		<title>Wikimedia Commons picture of the day for April 1</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/wiki/Special:FeedItem/potd/20180401000000/en"/>
		<updated>2018-04-01T00:00:00Z</updated>

		<summary type="html">&lt;div class=&quot;mw-parser-output&quot;&gt;&lt;div class=&quot;floatright&quot;&gt;
&lt;table class=&quot;toccolours&quot; style=&quot;width:300px;text-align:center&quot;&gt;
&lt;tr&gt;
&lt;th lang=&quot;en&quot;&gt;&lt;a href=&quot;/wiki/Commons:Picture_of_the_day&quot; title=&quot;Commons:Picture of the day&quot;&gt;Picture of the day&lt;/a&gt;
&lt;/th&gt;&lt;/tr&gt;
&lt;tr&gt;
&lt;td class=&quot;toccolours&quot; style=&quot;padding:0&quot;&gt;&lt;a href=&quot;/wiki/File:Mezquita_de_Nasirolmolk,_Shiraz,_Ir%C3%A1n,_2016-09-24,_DD_60-62_HDR.jpg&quot; class=&quot;image&quot;&gt;&lt;img alt=&quot;Mezquita de Nasirolmolk, Shiraz, Irán, 2016-09-24, DD 60-62 HDR.jpg&quot; src=&quot;https://upload.wikimedia.org/wikipedia/commons/thumb/d/de/Mezquita_de_Nasirolmolk%2C_Shiraz%2C_Ir%C3%A1n%2C_2016-09-24%2C_DD_60-62_HDR.jpg/300px-Mezquita_de_Nasirolmolk%2C_Shiraz%2C_Ir%C3%A1n%2C_2016-09-24%2C_DD_60-62_HDR.jpg&quot; width=&quot;300&quot; height=&quot;416&quot; srcset=&quot;https://upload.wikimedia.org/wikipedia/commons/thumb/d/de/Mezquita_de_Nasirolmolk%2C_Shiraz%2C_Ir%C3%A1n%2C_2016-09-24%2C_DD_60-62_HDR.jpg/450px-Mezquita_de_Nasirolmolk%2C_Shiraz%2C_Ir%C3%A1n%2C_2016-09-24%2C_DD_60-62_HDR.jpg 1.5x, https://upload.wikimedia.org/wikipedia/commons/thumb/d/de/Mezquita_de_Nasirolmolk%2C_Shiraz%2C_Ir%C3%A1n%2C_2016-09-24%2C_DD_60-62_HDR.jpg/600px-Mezquita_de_Nasirolmolk%2C_Shiraz%2C_Ir%C3%A1n%2C_2016-09-24%2C_DD_60-62_HDR.jpg 2x&quot; data-file-width=&quot;5630&quot; data-file-height=&quot;7798&quot; /&gt;&lt;/a&gt;
&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;
&lt;td&gt;&lt;span lang=&quot;en&quot; class=&quot;description en&quot;&gt;View of the arches in the interior of the &lt;a href=&quot;https://en.wikipedia.org/wiki/Nasir_ol_Molk_Mosque&quot; class=&quot;extiw&quot; title=&quot;en:Nasir ol Molk Mosque&quot;&gt;Nasir-ol-Molk Mosque&lt;/a&gt;, also known as the &lt;i&gt;Pink Mosque&lt;/i&gt;, which is a traditional mosque located in &lt;a href=&quot;https://en.wikipedia.org/wiki/Shiraz&quot; class=&quot;extiw&quot; title=&quot;en:Shiraz&quot;&gt;Shiraz&lt;/a&gt; district of Gowad-e-Arabān, &lt;a href=&quot;https://en.wikipedia.org/wiki/Iran&quot; class=&quot;extiw&quot; title=&quot;en:Iran&quot;&gt;Iran&lt;/a&gt;. The mosque was built from 1876 to 1888, by the order of Mirzā Hasan Ali (Nasir ol Molk), a &lt;a href=&quot;https://en.wikipedia.org/wiki/Qajar&quot; class=&quot;extiw&quot; title=&quot;en:Qajar&quot;&gt;Qajar&lt;/a&gt; ruler. &lt;/span&gt;&amp;#160;
&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/div&gt;
&lt;/div&gt;</summary>
		<author><name></name></author>
		
	</entry>
	<entry>
		<id>https://commons.wikimedia.org/wiki/Special:FeedItem/potd/20180402000000/en</id>
		<title>Wikimedia Commons picture of the day for April 2</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/wiki/Special:FeedItem/potd/20180402000000/en"/>
		<updated>2018-04-02T00:00:00Z</updated>

		<summary type="html">&lt;div class=&quot;mw-parser-output&quot;&gt;&lt;div class=&quot;floatright&quot;&gt;
&lt;table class=&quot;toccolours&quot; style=&quot;width:300px;text-align:center&quot;&gt;
&lt;tr&gt;
&lt;th lang=&quot;en&quot;&gt;&lt;a href=&quot;/wiki/Commons:Picture_of_the_day&quot; title=&quot;Commons:Picture of the day&quot;&gt;Picture of the day&lt;/a&gt;
&lt;/th&gt;&lt;/tr&gt;
&lt;tr&gt;
&lt;td class=&quot;toccolours&quot; style=&quot;padding:0&quot;&gt;&lt;a href=&quot;/wiki/File:M%C3%BCnster,_LVM_--_2017_--_9343-7.jpg&quot; class=&quot;image&quot;&gt;&lt;img alt=&quot;Münster, LVM -- 2017 -- 9343-7.jpg&quot; src=&quot;https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/M%C3%BCnster%2C_LVM_--_2017_--_9343-7.jpg/300px-M%C3%BCnster%2C_LVM_--_2017_--_9343-7.jpg&quot; width=&quot;300&quot; height=&quot;200&quot; srcset=&quot;https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/M%C3%BCnster%2C_LVM_--_2017_--_9343-7.jpg/450px-M%C3%BCnster%2C_LVM_--_2017_--_9343-7.jpg 1.5x, https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/M%C3%BCnster%2C_LVM_--_2017_--_9343-7.jpg/600px-M%C3%BCnster%2C_LVM_--_2017_--_9343-7.jpg 2x&quot; data-file-width=&quot;5471&quot; data-file-height=&quot;3648&quot; /&gt;&lt;/a&gt;
&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;
&lt;td&gt;&lt;span lang=&quot;en&quot; class=&quot;description en&quot;&gt;Office building of the &lt;a href=&quot;/wiki/Category:LVM_Versicherung&quot; title=&quot;Category:LVM Versicherung&quot;&gt;LVM&lt;/a&gt;, &lt;a href=&quot;/wiki/Category:M%C3%BCnster_(Westfalen)&quot; title=&quot;Category:Münster (Westfalen)&quot;&gt;Münster&lt;/a&gt;, &lt;a href=&quot;/wiki/North_Rhine-Westphalia&quot; class=&quot;mw-redirect&quot; title=&quot;North Rhine-Westphalia&quot;&gt;North Rhine-Westphalia&lt;/a&gt;, Germany &lt;/span&gt;&amp;#160;
&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/div&gt;
&lt;/div&gt;</summary>
		<author><name></name></author>
		
	</entry>
	<entry>
		<id>https://commons.wikimedia.org/wiki/Special:FeedItem/potd/20180403000000/en</id>
		<title>Wikimedia Commons picture of the day for April 3</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/wiki/Special:FeedItem/potd/20180403000000/en"/>
		<updated>2018-04-03T00:00:00Z</updated>

		<summary type="html">&lt;div class=&quot;mw-parser-output&quot;&gt;&lt;div class=&quot;floatright&quot;&gt;
&lt;table class=&quot;toccolours&quot; style=&quot;width:300px;text-align:center&quot;&gt;
&lt;tr&gt;
&lt;th lang=&quot;en&quot;&gt;&lt;a href=&quot;/wiki/Commons:Picture_of_the_day&quot; title=&quot;Commons:Picture of the day&quot;&gt;Picture of the day&lt;/a&gt;
&lt;/th&gt;&lt;/tr&gt;
&lt;tr&gt;
&lt;td class=&quot;toccolours&quot; style=&quot;padding:0&quot;&gt;&lt;a href=&quot;/wiki/File:Yellow-billed_oxpeckers_(Buphagus_africanus_africanus)_on_zebra.jpg&quot; class=&quot;image&quot;&gt;&lt;img alt=&quot;Yellow-billed oxpeckers (Buphagus africanus africanus) on zebra.jpg&quot; src=&quot;https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Yellow-billed_oxpeckers_%28Buphagus_africanus_africanus%29_on_zebra.jpg/300px-Yellow-billed_oxpeckers_%28Buphagus_africanus_africanus%29_on_zebra.jpg&quot; width=&quot;300&quot; height=&quot;200&quot; srcset=&quot;https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Yellow-billed_oxpeckers_%28Buphagus_africanus_africanus%29_on_zebra.jpg/450px-Yellow-billed_oxpeckers_%28Buphagus_africanus_africanus%29_on_zebra.jpg 1.5x, https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Yellow-billed_oxpeckers_%28Buphagus_africanus_africanus%29_on_zebra.jpg/600px-Yellow-billed_oxpeckers_%28Buphagus_africanus_africanus%29_on_zebra.jpg 2x&quot; data-file-width=&quot;2663&quot; data-file-height=&quot;1775&quot; /&gt;&lt;/a&gt;
&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;
&lt;td&gt;&lt;span lang=&quot;en&quot; class=&quot;description en&quot;&gt;&lt;a href=&quot;https://en.wikipedia.org/wiki/Yellow-billed_oxpecker&quot; class=&quot;extiw&quot; title=&quot;en:Yellow-billed oxpecker&quot;&gt;Yellow-billed oxpeckers&lt;/a&gt; (&lt;i&gt;&lt;a href=&quot;/wiki/Buphagus_africanus&quot; title=&quot;Buphagus africanus&quot;&gt;Buphagus africanus africanus&lt;/a&gt;&lt;/i&gt;) perch on a &lt;a href=&quot;https://en.wikipedia.org/wiki/zebra&quot; class=&quot;extiw&quot; title=&quot;w:zebra&quot;&gt;zebra&lt;/a&gt; in &lt;a href=&quot;https://en.wikipedia.org/wiki/Senegal&quot; class=&quot;extiw&quot; title=&quot;w:Senegal&quot;&gt;Senegal&lt;/a&gt;. Tomorrow, April 4, is Independence Day in &lt;a href=&quot;https://en.wikipedia.org/wiki/Senegal&quot; class=&quot;extiw&quot; title=&quot;w:Senegal&quot;&gt;Senegal&lt;/a&gt; &lt;/span&gt;&amp;#160;
&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/div&gt;
&lt;/div&gt;</summary>
		<author><name></name></author>
		
	</entry>
	<entry>
		<id>https://commons.wikimedia.org/wiki/Special:FeedItem/potd/20180404000000/en</id>
		<title>Wikimedia Commons picture of the day for April 4</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/wiki/Special:FeedItem/potd/20180404000000/en"/>
		<updated>2018-04-04T00:00:00Z</updated>

		<summary type="html">&lt;div class=&quot;mw-parser-output&quot;&gt;&lt;div class=&quot;floatright&quot;&gt;
&lt;table class=&quot;toccolours&quot; style=&quot;width:300px;text-align:center&quot;&gt;
&lt;tr&gt;
&lt;th lang=&quot;en&quot;&gt;&lt;a href=&quot;/wiki/Commons:Picture_of_the_day&quot; title=&quot;Commons:Picture of the day&quot;&gt;Picture of the day&lt;/a&gt;
&lt;/th&gt;&lt;/tr&gt;
&lt;tr&gt;
&lt;td class=&quot;toccolours&quot; style=&quot;padding:0&quot;&gt;&lt;a href=&quot;/wiki/File:Duisburg,_Landschaftspark_Duisburg-Nord_--_2016_--_1238-44.jpg&quot; class=&quot;image&quot;&gt;&lt;img alt=&quot;Duisburg, Landschaftspark Duisburg-Nord -- 2016 -- 1238-44.jpg&quot; src=&quot;https://upload.wikimedia.org/wikipedia/commons/thumb/0/01/Duisburg%2C_Landschaftspark_Duisburg-Nord_--_2016_--_1238-44.jpg/300px-Duisburg%2C_Landschaftspark_Duisburg-Nord_--_2016_--_1238-44.jpg&quot; width=&quot;300&quot; height=&quot;200&quot; srcset=&quot;https://upload.wikimedia.org/wikipedia/commons/thumb/0/01/Duisburg%2C_Landschaftspark_Duisburg-Nord_--_2016_--_1238-44.jpg/450px-Duisburg%2C_Landschaftspark_Duisburg-Nord_--_2016_--_1238-44.jpg 1.5x, https://upload.wikimedia.org/wikipedia/commons/thumb/0/01/Duisburg%2C_Landschaftspark_Duisburg-Nord_--_2016_--_1238-44.jpg/600px-Duisburg%2C_Landschaftspark_Duisburg-Nord_--_2016_--_1238-44.jpg 2x&quot; data-file-width=&quot;4943&quot; data-file-height=&quot;3295&quot; /&gt;&lt;/a&gt;
&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;
&lt;td&gt;&lt;span lang=&quot;en&quot; class=&quot;description en&quot;&gt;Hochofenstraße in the &lt;a href=&quot;/wiki/Category:Landschaftspark_Duisburg-Nord&quot; title=&quot;Category:Landschaftspark Duisburg-Nord&quot;&gt;Landschaftspark Duisburg-Nord&lt;/a&gt; in Duisburg, North Rhine-Westphalia, Germany &lt;/span&gt;&amp;#160;
&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/div&gt;
&lt;/div&gt;</summary>
		<author><name></name></author>
		
	</entry>
	<entry>
		<id>https://commons.wikimedia.org/wiki/Special:FeedItem/potd/20180405000000/en</id>
		<title>Wikimedia Commons picture of the day for April 5</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/wiki/Special:FeedItem/potd/20180405000000/en"/>
		<updated>2018-04-05T00:00:00Z</updated>

		<summary type="html">&lt;div class=&quot;mw-parser-output&quot;&gt;&lt;div class=&quot;floatright&quot;&gt;
&lt;table class=&quot;toccolours&quot; style=&quot;width:300px;text-align:center&quot;&gt;
&lt;tr&gt;
&lt;th lang=&quot;en&quot;&gt;&lt;a href=&quot;/wiki/Commons:Picture_of_the_day&quot; title=&quot;Commons:Picture of the day&quot;&gt;Picture of the day&lt;/a&gt;
&lt;/th&gt;&lt;/tr&gt;
&lt;tr&gt;
&lt;td class=&quot;toccolours&quot; style=&quot;padding:0&quot;&gt;&lt;a href=&quot;/wiki/File:Lysets_t%C3%B8ven.jpg&quot; class=&quot;image&quot;&gt;&lt;img alt=&quot;Lysets tøven.jpg&quot; src=&quot;https://upload.wikimedia.org/wikipedia/commons/thumb/6/68/Lysets_t%C3%B8ven.jpg/300px-Lysets_t%C3%B8ven.jpg&quot; width=&quot;300&quot; height=&quot;165&quot; srcset=&quot;https://upload.wikimedia.org/wikipedia/commons/thumb/6/68/Lysets_t%C3%B8ven.jpg/450px-Lysets_t%C3%B8ven.jpg 1.5x, https://upload.wikimedia.org/wikipedia/commons/thumb/6/68/Lysets_t%C3%B8ven.jpg/600px-Lysets_t%C3%B8ven.jpg 2x&quot; data-file-width=&quot;4597&quot; data-file-height=&quot;2525&quot; /&gt;&lt;/a&gt;
&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;
&lt;td&gt;&lt;span lang=&quot;en&quot; class=&quot;description en&quot;&gt;Diesel multiple units &lt;a href=&quot;https://en.wikipedia.org/wiki/IC4&quot; class=&quot;extiw&quot; title=&quot;w:IC4&quot;&gt;IC4&lt;/a&gt; under the &lt;a href=&quot;https://da.wikipedia.org/wiki/Ringgadebroen&quot; class=&quot;extiw&quot; title=&quot;da:Ringgadebroen&quot;&gt;Ringgade bridge&lt;/a&gt; with &quot;Hesitation of Light&quot; light installation in &lt;a href=&quot;https://en.wikipedia.org/wiki/Aarhus&quot; class=&quot;extiw&quot; title=&quot;w:Aarhus&quot;&gt;Aarhus&lt;/a&gt;, &lt;a href=&quot;https://en.wikipedia.org/wiki/Denmark&quot; class=&quot;extiw&quot; title=&quot;w:Denmark&quot;&gt;Denmark&lt;/a&gt; &lt;/span&gt;&amp;#160;
&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/div&gt;
&lt;/div&gt;</summary>
		<author><name></name></author>
		
	</entry>
	<entry>
		<id>https://commons.wikimedia.org/wiki/Special:FeedItem/potd/20180406000000/en</id>
		<title>Wikimedia Commons picture of the day for April 6</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/wiki/Special:FeedItem/potd/20180406000000/en"/>
		<updated>2018-04-06T00:00:00Z</updated>

		<summary type="html">&lt;div class=&quot;mw-parser-output&quot;&gt;&lt;div class=&quot;floatright&quot;&gt;
&lt;table class=&quot;toccolours&quot; style=&quot;width:300px;text-align:center&quot;&gt;
&lt;tr&gt;
&lt;th lang=&quot;en&quot;&gt;&lt;a href=&quot;/wiki/Commons:Picture_of_the_day&quot; title=&quot;Commons:Picture of the day&quot;&gt;Picture of the day&lt;/a&gt;
&lt;/th&gt;&lt;/tr&gt;
&lt;tr&gt;
&lt;td class=&quot;toccolours&quot; style=&quot;padding:0&quot;&gt;&lt;a href=&quot;/wiki/File:Sinfon%C3%ADa_de_las_Piedras,_valle_de_Garni,_Armenia,_2016-10-02,_DD_33.jpg&quot; class=&quot;image&quot;&gt;&lt;img alt=&quot;Sinfonía de las Piedras, valle de Garni, Armenia, 2016-10-02, DD 33.jpg&quot; src=&quot;https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Sinfon%C3%ADa_de_las_Piedras%2C_valle_de_Garni%2C_Armenia%2C_2016-10-02%2C_DD_33.jpg/300px-Sinfon%C3%ADa_de_las_Piedras%2C_valle_de_Garni%2C_Armenia%2C_2016-10-02%2C_DD_33.jpg&quot; width=&quot;300&quot; height=&quot;200&quot; srcset=&quot;https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Sinfon%C3%ADa_de_las_Piedras%2C_valle_de_Garni%2C_Armenia%2C_2016-10-02%2C_DD_33.jpg/450px-Sinfon%C3%ADa_de_las_Piedras%2C_valle_de_Garni%2C_Armenia%2C_2016-10-02%2C_DD_33.jpg 1.5x, https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Sinfon%C3%ADa_de_las_Piedras%2C_valle_de_Garni%2C_Armenia%2C_2016-10-02%2C_DD_33.jpg/600px-Sinfon%C3%ADa_de_las_Piedras%2C_valle_de_Garni%2C_Armenia%2C_2016-10-02%2C_DD_33.jpg 2x&quot; data-file-width=&quot;8688&quot; data-file-height=&quot;5792&quot; /&gt;&lt;/a&gt;
&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;
&lt;td&gt;&lt;span lang=&quot;en&quot; class=&quot;description en&quot;&gt;These well-preserved &lt;a href=&quot;https://en.wikipedia.org/wiki/basalt&quot; class=&quot;extiw&quot; title=&quot;en:basalt&quot;&gt;basalt&lt;/a&gt; columns are known as &quot;Symphony of the Stones&quot; and are located in the &lt;a href=&quot;https://en.wikipedia.org/wiki/Garni_Gorge&quot; class=&quot;extiw&quot; title=&quot;en:Garni Gorge&quot;&gt;Garni Gorge&lt;/a&gt;, near &lt;a href=&quot;https://en.wikipedia.org/wiki/Yerevan&quot; class=&quot;extiw&quot; title=&quot;en:Yerevan&quot;&gt;Yerevan&lt;/a&gt;, capital city of &lt;a href=&quot;https://en.wikipedia.org/wiki/Armenia&quot; class=&quot;extiw&quot; title=&quot;en:Armenia&quot;&gt;Armenia&lt;/a&gt;. The columns are visible because they were carved out by the &lt;a href=&quot;https://en.wikipedia.org/wiki/Goght_River&quot; class=&quot;extiw&quot; title=&quot;en:Goght River&quot;&gt;Goght River&lt;/a&gt;. &lt;/span&gt;&amp;#160;
&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/div&gt;
&lt;/div&gt;</summary>
		<author><name></name></author>
		
	</entry>
	<entry>
		<id>https://commons.wikimedia.org/wiki/Special:FeedItem/potd/20180407000000/en</id>
		<title>Wikimedia Commons picture of the day for April 7</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/wiki/Special:FeedItem/potd/20180407000000/en"/>
		<updated>2018-04-07T00:00:00Z</updated>

		<summary type="html">&lt;div class=&quot;mw-parser-output&quot;&gt;&lt;div class=&quot;floatright&quot;&gt;
&lt;table class=&quot;toccolours&quot; style=&quot;width:300px;text-align:center&quot;&gt;
&lt;tr&gt;
&lt;th lang=&quot;en&quot;&gt;&lt;a href=&quot;/wiki/Commons:Picture_of_the_day&quot; title=&quot;Commons:Picture of the day&quot;&gt;Picture of the day&lt;/a&gt;
&lt;/th&gt;&lt;/tr&gt;
&lt;tr&gt;
&lt;td class=&quot;toccolours&quot; style=&quot;padding:0&quot;&gt;&lt;a href=&quot;/wiki/File:Bronze_Horseman_02.jpg&quot; class=&quot;image&quot;&gt;&lt;img alt=&quot;Bronze Horseman 02.jpg&quot; src=&quot;https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/Bronze_Horseman_02.jpg/300px-Bronze_Horseman_02.jpg&quot; width=&quot;300&quot; height=&quot;200&quot; srcset=&quot;https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/Bronze_Horseman_02.jpg/450px-Bronze_Horseman_02.jpg 1.5x, https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/Bronze_Horseman_02.jpg/600px-Bronze_Horseman_02.jpg 2x&quot; data-file-width=&quot;4500&quot; data-file-height=&quot;3000&quot; /&gt;&lt;/a&gt;
&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;
&lt;td&gt;&lt;span lang=&quot;en&quot; class=&quot;description en&quot;&gt;The &lt;i&gt;&lt;a href=&quot;https://en.wikipedia.org/wiki/Bronze_Horseman&quot; class=&quot;extiw&quot; title=&quot;en:Bronze Horseman&quot;&gt;Bronze Horseman&lt;/a&gt;&lt;/i&gt; in &lt;a href=&quot;https://en.wikipedia.org/wiki/Saint_Petersburg&quot; class=&quot;extiw&quot; title=&quot;en:Saint Petersburg&quot;&gt;Saint Petersburg&lt;/a&gt;, &lt;a href=&quot;https://en.wikipedia.org/wiki/Russia&quot; class=&quot;extiw&quot; title=&quot;en:Russia&quot;&gt;Russia&lt;/a&gt;. &lt;/span&gt;&amp;#160;
&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/div&gt;
&lt;/div&gt;</summary>
		<author><name></name></author>
		
	</entry>
	<entry>
		<id>https://commons.wikimedia.org/wiki/Special:FeedItem/potd/20180408000000/en</id>
		<title>Wikimedia Commons picture of the day for April 8</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/wiki/Special:FeedItem/potd/20180408000000/en"/>
		<updated>2018-04-08T00:00:00Z</updated>

		<summary type="html">&lt;div class=&quot;mw-parser-output&quot;&gt;&lt;div class=&quot;floatright&quot;&gt;
&lt;table class=&quot;toccolours&quot; style=&quot;width:300px;text-align:center&quot;&gt;
&lt;tr&gt;
&lt;th lang=&quot;en&quot;&gt;&lt;a href=&quot;/wiki/Commons:Picture_of_the_day&quot; title=&quot;Commons:Picture of the day&quot;&gt;Picture of the day&lt;/a&gt;
&lt;/th&gt;&lt;/tr&gt;
&lt;tr&gt;
&lt;td class=&quot;toccolours&quot; style=&quot;padding:0&quot;&gt;&lt;a href=&quot;/wiki/File:Kyrenia_01-2017_img07_Castle_bastion.jpg&quot; class=&quot;image&quot;&gt;&lt;img alt=&quot;Kyrenia 01-2017 img07 Castle bastion.jpg&quot; src=&quot;https://upload.wikimedia.org/wikipedia/commons/thumb/4/4f/Kyrenia_01-2017_img07_Castle_bastion.jpg/300px-Kyrenia_01-2017_img07_Castle_bastion.jpg&quot; width=&quot;300&quot; height=&quot;150&quot; srcset=&quot;https://upload.wikimedia.org/wikipedia/commons/thumb/4/4f/Kyrenia_01-2017_img07_Castle_bastion.jpg/450px-Kyrenia_01-2017_img07_Castle_bastion.jpg 1.5x, https://upload.wikimedia.org/wikipedia/commons/thumb/4/4f/Kyrenia_01-2017_img07_Castle_bastion.jpg/600px-Kyrenia_01-2017_img07_Castle_bastion.jpg 2x&quot; data-file-width=&quot;5128&quot; data-file-height=&quot;2564&quot; /&gt;&lt;/a&gt;
&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;
&lt;td&gt;&lt;span lang=&quot;en&quot; class=&quot;description en&quot;&gt;The &lt;a href=&quot;https://en.wikipedia.org/wiki/Crenellation&quot; class=&quot;extiw&quot; title=&quot;en:Crenellation&quot;&gt;crenellated&lt;/a&gt; &lt;a href=&quot;https://en.wikipedia.org/wiki/Parapet&quot; class=&quot;extiw&quot; title=&quot;en:Parapet&quot;&gt;parapet&lt;/a&gt; on a bastion of &lt;a href=&quot;https://en.wikipedia.org/wiki/Kyrenia_Castle&quot; class=&quot;extiw&quot; title=&quot;en:Kyrenia Castle&quot;&gt;Kyrenia Castle&lt;/a&gt;, &lt;a href=&quot;https://en.wikipedia.org/wiki/Cyprus&quot; class=&quot;extiw&quot; title=&quot;en:Cyprus&quot;&gt;Cyprus&lt;/a&gt;. &lt;/span&gt;&amp;#160;
&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/div&gt;
&lt;/div&gt;</summary>
		<author><name></name></author>
		
	</entry>
	<entry>
		<id>https://commons.wikimedia.org/wiki/Special:FeedItem/potd/20180409000000/en</id>
		<title>Wikimedia Commons picture of the day for April 9</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/wiki/Special:FeedItem/potd/20180409000000/en"/>
		<updated>2018-04-09T00:00:00Z</updated>

		<summary type="html">&lt;div class=&quot;mw-parser-output&quot;&gt;&lt;div class=&quot;floatright&quot;&gt;
&lt;table class=&quot;toccolours&quot; style=&quot;width:300px;text-align:center&quot;&gt;
&lt;tr&gt;
&lt;th lang=&quot;en&quot;&gt;&lt;a href=&quot;/wiki/Commons:Picture_of_the_day&quot; title=&quot;Commons:Picture of the day&quot;&gt;Picture of the day&lt;/a&gt;
&lt;/th&gt;&lt;/tr&gt;
&lt;tr&gt;
&lt;td class=&quot;toccolours&quot; style=&quot;padding:0&quot;&gt;&lt;a href=&quot;/wiki/File:Pepe_Lopez_Peugeot_208_T16_(3).jpg&quot; class=&quot;image&quot;&gt;&lt;img alt=&quot;Pepe Lopez Peugeot 208 T16 (3).jpg&quot; src=&quot;https://upload.wikimedia.org/wikipedia/commons/thumb/4/45/Pepe_Lopez_Peugeot_208_T16_%283%29.jpg/300px-Pepe_Lopez_Peugeot_208_T16_%283%29.jpg&quot; width=&quot;300&quot; height=&quot;202&quot; srcset=&quot;https://upload.wikimedia.org/wikipedia/commons/thumb/4/45/Pepe_Lopez_Peugeot_208_T16_%283%29.jpg/450px-Pepe_Lopez_Peugeot_208_T16_%283%29.jpg 1.5x, https://upload.wikimedia.org/wikipedia/commons/thumb/4/45/Pepe_Lopez_Peugeot_208_T16_%283%29.jpg/600px-Pepe_Lopez_Peugeot_208_T16_%283%29.jpg 2x&quot; data-file-width=&quot;4336&quot; data-file-height=&quot;2924&quot; /&gt;&lt;/a&gt;
&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;
&lt;td&gt;&lt;span lang=&quot;en&quot; class=&quot;description en&quot;&gt;&lt;a href=&quot;https://en.wikipedia.org/wiki/Peugeot_208&quot; class=&quot;extiw&quot; title=&quot;w:Peugeot 208&quot;&gt;Peugeot 208&lt;/a&gt; T16 at Rally Serras de Fafe 2017 &lt;/span&gt;&amp;#160;
&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/div&gt;
&lt;/div&gt;</summary>
		<author><name></name></author>
		
	</entry>
	<entry>
		<id>https://commons.wikimedia.org/wiki/Special:FeedItem/potd/20180410000000/en</id>
		<title>Wikimedia Commons picture of the day for April 10</title>
		<link rel="alternate" type="text/html" href="https://commons.wikimedia.org/wiki/Special:FeedItem/potd/20180410000000/en"/>
		<updated>2018-04-10T00:00:00Z</updated>

		<summary type="html">&lt;div class=&quot;mw-parser-output&quot;&gt;&lt;div class=&quot;floatright&quot;&gt;
&lt;table class=&quot;toccolours&quot; style=&quot;width:300px;text-align:center&quot;&gt;
&lt;tr&gt;
&lt;th lang=&quot;en&quot;&gt;&lt;a href=&quot;/wiki/Commons:Picture_of_the_day&quot; title=&quot;Commons:Picture of the day&quot;&gt;Picture of the day&lt;/a&gt;
&lt;/th&gt;&lt;/tr&gt;
&lt;tr&gt;
&lt;td class=&quot;toccolours&quot; style=&quot;padding:0&quot;&gt;&lt;a href=&quot;/wiki/File:Lynx_lynx_-_05.jpg&quot; class=&quot;image&quot;&gt;&lt;img alt=&quot;Lynx lynx - 05.jpg&quot; src=&quot;https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Lynx_lynx_-_05.jpg/300px-Lynx_lynx_-_05.jpg&quot; width=&quot;300&quot; height=&quot;450&quot; srcset=&quot;https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Lynx_lynx_-_05.jpg/450px-Lynx_lynx_-_05.jpg 1.5x, https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Lynx_lynx_-_05.jpg/600px-Lynx_lynx_-_05.jpg 2x&quot; data-file-width=&quot;2432&quot; data-file-height=&quot;3648&quot; /&gt;&lt;/a&gt;
&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;
&lt;td&gt;&lt;span lang=&quot;en&quot; class=&quot;description en&quot;&gt;&lt;a href=&quot;https://en.wikipedia.org/wiki/Eurasian_lynx&quot; class=&quot;extiw&quot; title=&quot;w:Eurasian lynx&quot;&gt;Eurasian lynx&lt;/a&gt; (&lt;i&gt;Lynx lynx&lt;/i&gt;) in the &lt;a href=&quot;https://en.wikipedia.org/wiki/Zoo_of_Madrid&quot; class=&quot;extiw&quot; title=&quot;w:Zoo of Madrid&quot;&gt;Zoo of Madrid&lt;/a&gt;, &lt;a href=&quot;https://en.wikipedia.org/wiki/Spain&quot; class=&quot;extiw&quot; title=&quot;w:Spain&quot;&gt;Spain&lt;/a&gt;. &lt;/span&gt;&amp;#160;
&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/div&gt;
&lt;/div&gt;</summary>
		<author><name></name></author>
		
	</entry>
</feed>