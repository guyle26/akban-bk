// ==UserScript==
// @name         KITLV permalink retriever
// @namespace    http://veradekok.nl/
// @version      0.1
// @description  fish out permalink
// @author       1Veertje
// @match        http://*media-kitlv.nl/*
// @grant        none
// @require 
// ==/UserScript==
/* jshint -W097 */
'use strict';

// Your code here...
//$('#comment').val($('[property="og:url"]').attr('content'));
//$('#comment').attr('onClick', "this.focus();this.select()" );

var metas = document.getElementsByTagName('meta');
for each (var item in metas) {
    if (item.getAttribute('property') == 'og:url'){
       var a = item.getAttribute('content');
    }
}
document.getElementById('comment').set('value', a);
document.getElementById('comment').set('onClick', "this.focus();this.select()");